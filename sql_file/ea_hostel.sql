/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.8-MariaDB : Database - ea_hostel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ea_hostel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ea_hostel`;

/*Table structure for table `tbl_announcements` */

DROP TABLE IF EXISTS `tbl_announcements`;

CREATE TABLE `tbl_announcements` (
  `announcement_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL COMMENT 'tbl_users, admin OR user_admin',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = FALSE, 1 = TRUE',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  PRIMARY KEY (`announcement_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `tbl_announcements_ibfk_3` (`deleted_by`),
  CONSTRAINT `tbl_announcements_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_announcements_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_announcements_ibfk_3` FOREIGN KEY (`deleted_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_announcements` */

insert  into `tbl_announcements`(`announcement_id`,`title`,`description`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values 
(2,'qwe','qwe','published','2019-12-15 03:59:50',1,'2020-01-22 12:30:53',1,0,NULL,NULL),
(3,'qe','<p>qweqwe</p>','published','2019-12-15 04:00:35',1,NULL,NULL,1,'2019-12-15 04:21:59',1),
(4,'qwer','<p>qwe</p>','unpublished','2019-12-15 04:02:23',1,'2019-12-15 05:01:49',1,0,NULL,NULL),
(5,'123','123','unpublished','2019-12-25 09:26:48',1,NULL,NULL,0,NULL,NULL),
(6,'Hi','<p>I\'m bitch</p>','published','2020-01-22 10:35:49',1,NULL,NULL,0,NULL,NULL);

/*Table structure for table `tbl_blocks` */

DROP TABLE IF EXISTS `tbl_blocks`;

CREATE TABLE `tbl_blocks` (
  `block_id` int(11) NOT NULL AUTO_INCREMENT,
  `building_id` int(11) NOT NULL COMMENT 'tbl_building',
  `name` varchar(10) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `number_of_person` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0 = Inactive, 1 = Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL COMMENT 'tbl_users, admin OR user_admin',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = FALSE, 1 = TRUE',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  PRIMARY KEY (`block_id`),
  KEY `building_id` (`building_id`),
  KEY `tbl_blocks_ibfk_2` (`created_by`),
  KEY `tbl_blocks_ibfk_3` (`updated_by`),
  KEY `tbl_blocks_ibfk_4` (`deleted_by`),
  CONSTRAINT `tbl_blocks_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `tbl_buildings` (`building_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_blocks_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_blocks_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_blocks_ibfk_4` FOREIGN KEY (`deleted_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_blocks` */

insert  into `tbl_blocks`(`block_id`,`building_id`,`name`,`price`,`number_of_person`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values 
(2,1,'A',250.00,3,1,'2019-12-22 00:01:47',1,'2019-12-22 13:28:58',1,0,NULL,NULL),
(3,1,'B',350.00,2,1,'2019-12-22 00:17:54',1,'2019-12-22 00:17:56',1,0,NULL,NULL),
(4,1,'C',250.00,3,1,'2019-12-22 00:18:08',1,NULL,NULL,0,NULL,NULL),
(5,5,'A',250.00,3,1,'2019-12-22 00:31:04',1,NULL,NULL,0,NULL,NULL);

/*Table structure for table `tbl_buildings` */

DROP TABLE IF EXISTS `tbl_buildings`;

CREATE TABLE `tbl_buildings` (
  `building_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `type` char(1) NOT NULL COMMENT 'for differentiate gender',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '0 = Inactive, 1 = Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL COMMENT 'tbl_users, admin OR user_admin',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = FALSE, 1 = TRUE',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  PRIMARY KEY (`building_id`),
  KEY `tbl_buildings_ibfk_1` (`created_by`),
  KEY `tbl_buildings_ibfk_2` (`updated_by`),
  KEY `tbl_buildings_ibfk_3` (`deleted_by`),
  CONSTRAINT `tbl_buildings_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_buildings_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_buildings_ibfk_3` FOREIGN KEY (`deleted_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_buildings` */

insert  into `tbl_buildings`(`building_id`,`name`,`type`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values 
(1,'HB1','M',1,'2019-12-21 21:04:53',1,'2019-12-22 13:24:04',1,0,NULL,NULL),
(5,'HB2','M',1,'2019-12-22 00:18:27',1,NULL,NULL,0,NULL,NULL),
(6,'HB3','F',1,'2019-12-22 00:33:37',1,NULL,NULL,0,NULL,NULL);

/*Table structure for table `tbl_feedbacks` */

DROP TABLE IF EXISTS `tbl_feedbacks`;

CREATE TABLE `tbl_feedbacks` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL COMMENT 'tbl_users, user',
  `replied_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  PRIMARY KEY (`feedback_id`),
  KEY `tbl_feedbacks_ibfk_1` (`created_by`),
  KEY `tbl_feedbacks_ibfk_2` (`replied_by`),
  CONSTRAINT `tbl_feedbacks_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_feedbacks_ibfk_2` FOREIGN KEY (`replied_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_feedbacks` */

insert  into `tbl_feedbacks`(`feedback_id`,`title`,`description`,`created_at`,`created_by`,`replied_by`) values 
(1,'test','test','2019-12-24 23:43:41',2,1),
(5,'12314','2454234</p><p><span style=\"background-color: rgb(255, 255, 0);\">dsfsdfsdf</span></p><p><span style=\"background-color: rgb(255, 255, 0);\"><b>dsfsdfsdf</b></span','2019-12-29 16:27:26',2,NULL),
(6,'123','<p>afasdasd</p>','2019-12-29 16:36:22',2,1),
(7,'qwe','<p style=\"line-height: 1.2;\">qwe</p>','2020-01-01 02:15:32',2,NULL),
(8,'asd','<p>asd</p>','2020-02-16 22:45:10',5,1),
(9,'eee','<p>eee</p>','2020-02-16 22:47:01',5,1);

/*Table structure for table `tbl_floors` */

DROP TABLE IF EXISTS `tbl_floors`;

CREATE TABLE `tbl_floors` (
  `floor_id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) NOT NULL COMMENT 'tbl_blocks',
  `name` varchar(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0 = Inactive, 1 = Active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL COMMENT 'tbl_users, admin OR user_admin',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  PRIMARY KEY (`floor_id`),
  KEY `block_id` (`block_id`),
  KEY `tbl_floors_ibfk_2` (`created_by`),
  KEY `tbl_floors_ibfk_3` (`updated_by`),
  CONSTRAINT `tbl_floors_ibfk_1` FOREIGN KEY (`block_id`) REFERENCES `tbl_blocks` (`block_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_floors_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_floors_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_floors` */

insert  into `tbl_floors`(`floor_id`,`block_id`,`name`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,2,'1',1,'2019-12-22 17:29:55',1,'2019-12-22 17:33:31',1),
(2,2,'2',1,'2019-12-22 17:30:51',1,NULL,NULL),
(4,2,'3',1,'2019-12-29 02:14:03',1,'2020-02-12 01:27:05',1),
(5,3,'1',1,'2020-02-12 01:16:32',1,NULL,NULL);

/*Table structure for table `tbl_notifications` */

DROP TABLE IF EXISTS `tbl_notifications`;

CREATE TABLE `tbl_notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'tbl_user, all',
  `title` varchar(50) NOT NULL,
  `message` varchar(255) NOT NULL,
  `link` varchar(100) NOT NULL DEFAULT '#',
  `seen` tinyint(1) NOT NULL DEFAULT 0,
  `seen_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL COMMENT 'tbl_user, all',
  PRIMARY KEY (`notification_id`),
  KEY `tbl_notifications_ibfk_1` (`user_id`),
  KEY `tbl_notifications_ibfk_2` (`created_by`),
  CONSTRAINT `tbl_notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_notifications_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_notifications` */

insert  into `tbl_notifications`(`notification_id`,`user_id`,`title`,`message`,`link`,`seen`,`seen_at`,`created_at`,`created_by`) values 
(1,1,'Feedback','Lim Chun Yuan submitted a feedback.','admin/feedbacks/list',1,'2019-12-29 16:36:26','2019-12-29 16:36:22',2),
(2,3,'Feedback','Lim Chun Yuan submitted a feedback.','admin/feedbacks/list',0,NULL,'2019-12-29 16:36:22',2),
(3,2,'Reply Feedback','Administrator reply your feedback. Please check your email.','#',1,'2019-12-29 16:46:16','2019-12-29 16:42:56',1),
(4,1,'Feedback','Lim Chun Yuan submitted a feedback.','admin/feedbacks/list',1,'2020-01-01 02:15:34','2020-01-01 02:15:32',2),
(5,3,'Feedback','Lim Chun Yuan submitted a feedback.','admin/feedbacks/list',0,NULL,'2020-01-01 02:15:32',2);

/*Table structure for table `tbl_reply_feedbacks` */

DROP TABLE IF EXISTS `tbl_reply_feedbacks`;

CREATE TABLE `tbl_reply_feedbacks` (
  `reply_feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `feedback_id` int(11) NOT NULL COMMENT 'tbl_feedbacks',
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL COMMENT 'tbl_users, admin OR user_admin',
  PRIMARY KEY (`reply_feedback_id`),
  KEY `tbl_reply_feedbacks_ibfk_1` (`feedback_id`),
  KEY `tbl_reply_feedbacks_ibfk_2` (`created_by`),
  CONSTRAINT `tbl_reply_feedbacks_ibfk_1` FOREIGN KEY (`feedback_id`) REFERENCES `tbl_feedbacks` (`feedback_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_reply_feedbacks_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_reply_feedbacks` */

insert  into `tbl_reply_feedbacks`(`reply_feedback_id`,`feedback_id`,`subject`,`message`,`created_at`,`created_by`) values 
(1,1,'123','123','2019-12-29 02:30:55',1),
(2,6,'sad','sdas</p><p>da</p><p>sd</p><p>asd</p><p>as</p><p>da</p><p>sdas</p><p>d','2019-12-29 16:42:56',1),
(3,8,'test','<p>test</p>','2020-02-16 22:45:48',1),
(4,9,'ee','<p>ee</p>','2020-02-16 22:47:08',1);

/*Table structure for table `tbl_request_room` */

DROP TABLE IF EXISTS `tbl_request_room`;

CREATE TABLE `tbl_request_room` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL COMMENT 'tbl_rooms',
  `file` varchar(200) NOT NULL,
  `status` varchar(50) NOT NULL,
  `submitted_by` int(11) NOT NULL COMMENT 'tbl_users, user',
  `submitted_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `room_id` (`room_id`),
  KEY `submitted_by` (`submitted_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `tbl_request_room_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `tbl_rooms` (`room_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_request_room_ibfk_2` FOREIGN KEY (`submitted_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_request_room_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_request_room` */

insert  into `tbl_request_room`(`request_id`,`room_id`,`file`,`status`,`submitted_by`,`submitted_at`,`updated_by`,`updated_at`) values 
(1,3,'/uploads/2_receipt.jpg','accepted',2,'2020-02-16 14:26:28',1,'2020-02-16 19:57:38'),
(2,3,'/uploads/5_receipt.jpg','accepted',5,'2020-02-16 14:57:04',1,'2020-02-16 22:47:34');

/*Table structure for table `tbl_roles` */

DROP TABLE IF EXISTS `tbl_roles`;

CREATE TABLE `tbl_roles` (
  `keyword` varchar(20) NOT NULL,
  `description` varchar(20) NOT NULL,
  PRIMARY KEY (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_roles` */

insert  into `tbl_roles`(`keyword`,`description`) values 
('administrator','Administrator'),
('user','User'),
('user_admin','User Admin');

/*Table structure for table `tbl_rooms` */

DROP TABLE IF EXISTS `tbl_rooms`;

CREATE TABLE `tbl_rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `floor_id` int(11) NOT NULL COMMENT 'tbl_floors',
  `name` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL COMMENT 'tbl_users, admin OR user_admin',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'tbl_users, admin OR user_admin',
  PRIMARY KEY (`room_id`),
  KEY `floor_id` (`floor_id`),
  KEY `tbl_rooms_ibfk_2` (`created_by`),
  KEY `tbl_rooms_ibfk_3` (`updated_by`),
  CONSTRAINT `tbl_rooms_ibfk_1` FOREIGN KEY (`floor_id`) REFERENCES `tbl_floors` (`floor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_rooms_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_rooms_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_rooms` */

insert  into `tbl_rooms`(`room_id`,`floor_id`,`name`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,1,'1',0,'2020-02-12 01:12:04',1,'2020-02-12 01:30:50',1),
(2,1,'2',1,'2020-02-12 01:12:09',1,'2020-02-12 01:31:03',1),
(3,5,'1',1,'2020-02-12 01:16:39',1,NULL,NULL),
(4,1,'3',1,'2020-02-12 01:24:52',1,NULL,NULL);

/*Table structure for table `tbl_student_details` */

DROP TABLE IF EXISTS `tbl_student_details`;

CREATE TABLE `tbl_student_details` (
  `user_id` int(11) NOT NULL COMMENT 'tbl_users, user',
  `student_id` varchar(20) NOT NULL,
  `school_name` varchar(200) NOT NULL,
  KEY `user_id` (`user_id`),
  CONSTRAINT `tbl_student_details_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_student_details` */

insert  into `tbl_student_details`(`user_id`,`student_id`,`school_name`) values 
(2,'1171200943','MMU'),
(5,'1231321321','Taylor'),
(6,'1231231232','IMU'),
(7,'1231231233','TARC');

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_keyword` varchar(20) NOT NULL COMMENT 'tbl_roles',
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `gender` char(1) NOT NULL,
  `nric` varchar(12) NOT NULL,
  `handphone_number` varchar(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `request_key` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'tbl_users',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = FALSE, 1 = TRUE',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL COMMENT 'tbl_users',
  PRIMARY KEY (`user_id`),
  KEY `role_keyword` (`role_keyword`),
  KEY `tbl_users_ibfk_2` (`created_by`),
  KEY `tbl_users_ibfk_3` (`updated_by`),
  KEY `tbl_users_ibfk_4` (`deleted_by`),
  CONSTRAINT `tbl_users_ibfk_1` FOREIGN KEY (`role_keyword`) REFERENCES `tbl_roles` (`keyword`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_users_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_users_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_users_ibfk_4` FOREIGN KEY (`deleted_by`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`user_id`,`role_keyword`,`username`,`password`,`fullname`,`gender`,`nric`,`handphone_number`,`email`,`request_key`,`last_login`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values 
(1,'administrator','admin','922569135331794390ea969b2beac7702df32986','Administrator','M','991231109999','0123456789','admin@ea_hostel.com',NULL,'2020-02-16 22:35:13','2019-12-12 09:26:43',NULL,'2020-02-16 22:35:13',1,0,NULL,NULL),
(2,'user','user_a','922569135331794390ea969b2beac7702df32986','User A','M','990116107869','0123456789','user_a@ea_hostel.com',NULL,'2020-02-16 11:36:19','2019-12-12 09:26:43',NULL,'2020-02-16 11:36:19',2,0,NULL,NULL),
(3,'user_admin','user_admin','922569135331794390ea969b2beac7702df32986','User Admin','M','990116107869','0123456789','user_admin@ea_hostel.com',NULL,'2019-12-17 00:24:53','2019-12-16 22:16:08',1,'2019-12-29 02:48:54',1,0,NULL,NULL),
(5,'user','user_b','922569135331794390ea969b2beac7702df32986','User B','M','990116107869','0123456789','lim116@yahoo.com',NULL,'2020-02-16 22:45:02','2020-02-12 01:04:31',NULL,'2020-02-16 22:45:02',5,0,NULL,NULL),
(6,'user','user_c','922569135331794390ea969b2beac7702df32986','User C','M','990116107869','0123456789','user_c@ea_hostel.com',NULL,'2020-02-16 23:39:13','2020-02-12 01:05:19',NULL,'2020-02-16 23:39:13',6,0,NULL,NULL),
(7,'user','user_d','922569135331794390ea969b2beac7702df32986','User D','M','990116107869','0123456789','user_d@ea_hostel.com',NULL,NULL,'2020-02-12 01:05:42',NULL,NULL,NULL,0,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
