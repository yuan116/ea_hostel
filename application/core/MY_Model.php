<?php

class MY_Model extends CI_Model
{
    protected $_table_name;
    protected $_primary_key;
    protected $_order_by;

    protected $_created_by = 'created_by';
    protected $_created_date = 'created_at';

    protected $_updated_by = 'updated_by';
    protected $_updated_date = 'updated_at';

    protected $_deleted_by = 'deleted_by';
    protected $_deleted_date = 'deleted_at';

    public function __construct()
    {
        parent::__construct();
    }

    public function set_property($properties, string $data = NULL): void
    {
        if (is_array($properties))
        {
            foreach($properties as $key => $value)
            {
                $this->{"_{$key}"} = $value;
            }
        }
        else
        {
            $this->{"_{$properties}"} = $data;
        }
    }

    public function check_data(array $where): bool
    {
        $return = TRUE;

        $data = array_values($where);
        foreach ($data as $value) {
            if (!empty($value)) {
                $return = FALSE;
            } else {
                $return = TRUE;
                break;
            }
        }

        if ($return === FALSE) {
            $this->db->select($this->_primary_key);
            $this->db->from($this->_table_name);
            $this->db->where($where);
            $this->db->where('deleted', 0);
            $row = $this->db->get()->row();

            $return = TRUE;
            if (!empty($row)) {
                $return = FALSE;
                if (!empty($this->input->post($this->_primary_key))) {
                    if ($row->{$this->_primary_key} == $this->input->post($this->_primary_key)) {
                        $return = TRUE;
                    }
                }
            }
        }

        return $return;
    }

    public function save(array $data, $id = NULL)
    {
        if (empty($id)) {
            $this->_set_save_value('created', $data);

            $this->db->insert($this->_table_name, $data);
            return $this->db->insert_id();
        } else {
            $this->_set_save_value('updated', $data);

            return $this->db->update($this->_table_name, $data, array($this->_primary_key => $id));
        }
    }

    private function _set_save_value(string $type, array &$data)
    {
        if ($this->db->field_exists($this->{"_{$type}_by"}, $this->_table_name)) {
            $data[$this->{"_{$type}_by"}] = $this->session->userdata('user_id');
            $data[$this->{"_{$type}_date"}] = date('Y-m-d H:i:s');
        }
    }

    public function delete($id, $delete = FALSE): bool
    {
        if ($delete === TRUE || ($this->db->field_exists('deleted', $this->_table_name) === FALSE)) {
            return $this->db->delete($this->_table_name, array($this->_primary_key => $id));
        } else {
            $data = array(
                'deleted' => TRUE,
                $this->_deleted_by => $this->session->userdata('user_id'),
                $this->_deleted_date => date('Y-m-d H:i:s')
            );

            return $this->save($data, $id);
        }
    }

    public function my_dtb($obj, bool $by_having = FALSE): void
    {
        $offset     = $obj['start'];
        $limit      = $obj['length'];
        $columns    = $obj['columns'];
        $order      = isset($obj['order']) ? $obj['order'] : array();
        $search     = $obj['search'];
        $q_group    = FALSE;

        foreach ($columns as $c) {
            if (!empty($c['name'])) {
                if (!empty($search['value'])) {
                    if ($by_having === TRUE) {
                        $this->db->or_having("{$c['name']} LIKE '%{$search['value']}%'");
                    } else {
                        if ($q_group === FALSE) {
                            $this->db->group_start();
                            $q_group = TRUE;
                        }

                        $this->db->or_like($c['name'], $search['value']);
                    }
                }

                if (!empty($c['search']['value'])) {
                    if ($by_having === TRUE) {
                        $this->db->or_having("{$c['name']} LIKE '%{$c['search']['value']}%'");
                    } else {
                        if ($q_group === FALSE) {
                            $this->db->group_start();
                            $q_group = TRUE;
                        }

                        $this->db->or_like($c['name'], $c['search']['value']);
                    }
                }
            }
        }

        if ($q_group === TRUE) {
            $this->db->group_end();
        }

        if (!empty($order)) {
            foreach ($order as $o) {
                if (!empty($columns[$o['column']]['name'])) {
                    $this->db->order_by($columns[$o['column']]['name'], $o['dir']);
                }
            }
        }

        $this->db->limit($limit, $offset);
    }

    public function my_dtb_count($obj, bool $by_having = FALSE): void
    {
        $columns = $obj['columns'];
        $search  = $obj['search'];
        $q_group = FALSE;

        foreach ($columns as $c) {
            if (!empty($c['name'])) {
                if (!empty($search['value'])) {
                    if ($by_having === TRUE) {
                        $this->db->or_having("{$c['name']} LIKE '%{$search['value']}%'");
                    } else {
                        if ($q_group === FALSE) {
                            $this->db->group_start();
                            $q_group = TRUE;
                        }

                        $this->db->or_like($c['name'], $search['value']);
                    }
                }

                if (!empty($c['search']['value'])) {
                    if ($by_having === TRUE) {
                        $this->db->or_having("{$c['name']} LIKE '%{$c['search']['value']}%'");
                    } else {
                        if ($q_group === FALSE) {
                            $this->db->group_start();
                            $q_group = TRUE;
                        }

                        $this->db->or_like($c['name'], $c['search']['value']);
                    }
                }
            }
        }

        if ($q_group === TRUE) {
            $this->db->group_end();
        }
    }
}
