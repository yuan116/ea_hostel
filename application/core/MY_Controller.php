<?php

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_check_login();
    }

    public function check_username()
    {
        $data = array();
        $this->load->model('user_model');

        if ($this->user_model->check_data(array('username' => $this->input->post('username'))) === FALSE) {
            $data['message'] = 'This Username has been registered by other user.';
            $data['result']  = FALSE;
        } else {
            $data['message'] = '';
            $data['result']  = TRUE;
        }

        if (isset($_POST['type'])) {
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->form_validation->set_message('check_username', $data['message']);
            return $data['result'];
        }
    }

    public function error_404(): void
    {
        $data['content'] = $this->load->view('public/error_404', NULL, TRUE);

        $this->load->view('html_layout', $data);
    }

    protected function _redirect_404()
    {
        log_message('error', "Page Not Found: {$this->uri->uri_string()}");
        redirect('error_404');
    }

    private function _check_login(): void
    {
        $url_rsegment = $this->uri->rsegment(1);
        $url_segment  = $this->uri->segment(1);

        if ($url_segment != 'logout') {
            if ($this->session->has_userdata('loggedin')) {
                $role_keyword = $this->session->userdata('role_keyword');

                if ($url_rsegment == 'public_controller') {
                    $this->_redirect_loggedin();
                } else if (($role_keyword == ROLE_ADMINISTRATOR or $role_keyword == ROLE_USER_ADMIN) && $url_segment != 'admin') {
                    redirect('admin/error_404');
                } else if ($role_keyword == ROLE_USER && $url_segment != 'user') {
                    redirect('user/error_404');
                }
            } else {
                if ($url_rsegment != 'public_controller' or $url_segment == 'public_controller') {
                    redirect();
                }
            }
        }
    }

    protected function _get_hash_password(string $password): string
    {
        return hash('sha1', "A{$password}Z");
    }

    protected function _redirect_loggedin(): void
    {
        $role_keyword = $this->session->userdata('role_keyword');

        if ($role_keyword == ROLE_ADMINISTRATOR or $role_keyword == ROLE_USER_ADMIN) {
            redirect('admin/dashboard');
        } else if ($role_keyword == ROLE_USER) {
            redirect('user/dashboard');
        }
    }

    /* protected function _save_notification(array $data, $to): void
    {
        $this->load->model('user_model');

        if ($to == 'ALL_ADMIN') {
            $users_list = $this->user_model->get_all_admin_list();

            foreach ($users_list as $user) {
                $data['user_id'] = $user->user_id;
                $this->user_model->save_notification($data);
            }
        } else {
            $data['user_id'] = $to;
            $this->user_model->save_notification($data);
        }
    } */

    protected function _set_login_session($user_data): void
    {
        $this->session->set_userdata(array(
            'user_id'          => $user_data->user_id,
            'role_keyword'     => $user_data->role_keyword,
            'username'         => $user_data->username,
            'fullname'         => $user_data->fullname,
            'gender'           => $user_data->gender,
            'handphone_number' => $user_data->handphone_number,
            'email'            => $user_data->email,
            'nric'             => $user_data->nric,
            'loggedin'         => TRUE
        ));

        if ($user_data->role_keyword == ROLE_USER) {
            $this->session->set_userdata(array(
                'student_id'  => $user_data->student_id,
                'school_name' => $user_data->school_name
            ));
        }
    }

    protected function _send_mail(array $email_data): bool
    {
        $this->load->library('email');

        $this->email->from('ea_hostel@ea_hostel.com', 'EA Hostel');
        $this->email->to($email_data['to']);
        $this->email->cc('lim116@yahoo.com');
        $this->email->subject($email_data['subject']);
        $this->email->message($email_data['message']);

        return $this->email->send();
    }

    protected function _get_upload_file_path($file_name, $config = NULL)
    {
        $file_config = array(
            'upload_path'            => 'uploads/',
            'allowed_types'          => NULL,
            'file_name'              => NULL,
            'file_ext_tolower'       => TRUE,
            'overwrite'              => FALSE,
            'max_size'               => 0,
            'max_width'              => 0,
            'max_height'             => 0,
            'min_width'              => 0,
            'min_height'             => 0,
            'max_filename'           => 0,
            'max_filename_increment' => 100,
            'encrypt_name'           => FALSE,
            'remove_spaces'          => TRUE,
            'detect_mime'            => TRUE,
            'mod_mime_fix'           => TRUE
        );

        if (!empty($config)) {
            foreach ($config as $key => $value) {
                $file_config[$key] = $value;
            }
        }

        $this->load->library('upload', $file_config);

        if ($this->upload->do_upload($file_name)) {
            return "/uploads/{$this->upload->data('file_name')}";
        } else {
            log_message('error', $this->upload->display_errors());
        }
    }
}
