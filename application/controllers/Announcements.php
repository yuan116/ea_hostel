<?php

class Announcements extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('announcements_model');
    }

    public function list(): void
    {
        $template_data['tab_active'] = 1;
        $template_data['tab_view']   = $this->load->view('admin/announcements/announcements_list', NULL, TRUE);
        $data['content'] = $this->load->view('admin/announcements/announcements_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    public function get_list(): void
    {
        $data['data']         = $this->announcements_model->get_list();
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function form($announcement_id = NULL): void
    {
        if (!empty($_POST) && isset($_POST)) {
            if ($this->_save_form($announcement_id) === TRUE) {
                redirect('admin/announcements/list');
            }
        }

        $tab_view = array();
        if (!empty($announcement_id)) {
            $tab_view['data'] = $this->announcements_model->get_details($announcement_id);

            if (empty($tab_view['data'])) {
                $this->_redirect_404();
            }
        }

        $template_data['tab_active'] = 2;
        $template_data['tab_view']   = $this->load->view('admin/announcements/announcements_form', $tab_view, TRUE);
        $data['content'] = $this->load->view('admin/announcements/announcements_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _save_form($announcement_id): bool
    {
        $result = FALSE;

        $data = $this->input->post(array('title', 'description', 'status'));
        $data['description'] = str_replace('<p><br></p>', '', $data['description']);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules(array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|required|max_length[100]|callback_check_title'
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            if (empty($announcement_id)) {
                $this->announcements_model->save($data);
            } else {
                $this->announcements_model->save($data, $announcement_id);
            }

            $result = TRUE;
        }

        return $result;
    }

    public function check_title(): bool
    {
        $result = $this->announcements_model->check_data(array('title' => $this->input->post('title')));

        if ($result === FALSE) {
            $this->form_validation->set_message('check_title', 'This Title has been used by other announcements.');
        }

        return $result;
    }

    public function change_status(): void
    {
        $this->announcements_model->save(array('status' => $this->input->post('status')), $this->input->post('announcement_id'));
    }

    public function delete(int $announcement_id): void
    {
        if (empty($this->announcements_model->get_details($announcement_id))) {
            $this->_redirect_404();
        }

        $this->announcements_model->delete($announcement_id, TRUE);

        redirect('admin/announcements/list');
    }
}
