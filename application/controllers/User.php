<?php

class User extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function list(): void
    {
        $template_data['tab_active'] = 1;
        $template_data['tab_view']   = $this->load->view('admin/user/user_list', NULL, TRUE);
        $data['content'] = $this->load->view('admin/user/user_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    public function get_list(): void
    {
        $data['data']         = $this->user_model->get_list();
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function form($user_id = NULL): void
    {
        if ($user_id == $this->session->userdata('user_id') or $user_id == 1) {
            $this->_redirect_404();
        }

        if (!empty($_POST) && isset($_POST)) {
            if ($this->_save_form($user_id) === TRUE) {
                redirect('admin/user/list');
            }
        }

        $tab_view = array();
        if (!empty($user_id)) {
            $tab_view['data'] = $this->user_model->get_details($user_id);

            if (empty($tab_view)) {
                $this->_redirect_404();
            }
        }

        $template_data['tab_active'] = 2;
        $template_data['tab_view']   = $this->load->view('admin/user/user_form', $tab_view, TRUE);
        $data['content'] = $this->load->view('admin/user/user_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _save_form($user_id): bool
    {
        $result = FALSE;

        $rules = array(
            array(
                'field' => 'role_keyword',
                'label' => 'Role',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ),
            array(
                'field' => 'nric',
                'label' => 'Identity Card Number',
                'rules' => 'trim|integer|required|min_length[12]|max_length[12]'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|max_length[255]'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|min_length[10]|max_length[11]'
            )
        );

        if (empty($user_id)) {
            $rules[] = array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|alpha_dash|max_length[50]|callback_check_username'
            );
        }

        if ($this->input->post('role_keyword') == ROLE_USER) {
            $rules[] = array(
                'field' => 'student_id',
                'label' => 'Student ID',
                'rules' => 'trim|required|max_length[20]'
            );
            $rules[] = array(
                'field' => 'school_name',
                'label' => 'University/College Name',
                'rules' => 'trim|required|max_length[200]'
            );
        }

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === TRUE) {
            $user_data = $this->input->post(array('role_keyword', 'fullname', 'gender', 'nric', 'handphone_number', 'email'));

            if (empty($user_id)) {
                $user_data['username'] = $this->input->post('username');
                $user_data['password'] = random_string();
                $this->_send_welcome_admin_email($user_data);

                $user_data['password'] = $this->_get_hash_password($user_data['password']);

                $this->user_model->save($user_data);
            } else {
                $this->user_model->save($user_data, $user_id);

                if ($user_data['role_keyword'] == ROLE_USER) {
                    $this->user_model->save_student_details($this->input->post(array('student_id', 'school_name')), $user_id);
                }
            }

            $result = TRUE;
        }

        return $result;
    }

    private function _send_welcome_admin_email(array $user_data): bool
    {
        $message = 'Hi {fullname},<br /><br />
                    Welcome to EA Hostel Admin Management.<br /><br />
                    Below are your user login information.<br />
                    Username: {username} <br />
                    Password: {password} <br />
                    <a href="{link}">EA Hostel Website</a> <br />
                    
                    If we suspect and found out you try to make trouble to our system. <br />
                    We will remove you from our system.';

        $message = str_replace('{fullname}', $user_data['fullname'], $message);
        $message = str_replace('{username}', $user_data['username'], $message);
        $message = str_replace('{password}', $user_data['password'], $message);
        $message = str_replace('{link}', site_url(), $message);

        $email_data['to']      = $user_data['email'];
        $email_data['subject'] = 'Welcome to EA Hostel';
        $email_data['message'] = $message;

        return $this->_send_mail($email_data);
    }

    public function delete(int $user_id): void
    {
        if ($user_id == $this->session->userdata('user_id') or $user_id == 1 or empty($this->user_model->get_details($user_id))) {
            $this->_redirect_404();
        }

        $this->user_model->delete($user_id, TRUE);

        redirect('admin/user/list');
    }

    public function get_request_user_details()
    {
        $data = $this->user_model->get_request_user_details($this->input->post('user_id'));

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /* public function view_notification()
    {
        $data['content'] = $this->load->view('public/view_notification', NULL, TRUE);

        $this->load->view('html_layout', $data);
    }

    public function load_notifications_list()
    {
        $all_notifications = $this->user_model->get_notifications(20, $this->input->post('offset'));

        $output = '';
        foreach ($all_notifications as $notification) {

            $time_different = floor(abs(strtotime(date('Y-m-d H:i:s')) - strtotime($notification->created_at)) / 60);

            $icon = 'fa-calendar';
            $date = $notification->created_at;
            if ($time_different < 10080) {
                $icon = 'fa-clock-o';
                $date = timespan(strtotime($notification->created_at), time(), 1) . ' ago';
            }

            $output .= '<tr>';
            $output .=     '<td>';
            $output .=          "<span class='notification_title'>{$notification->title}</span> : <span class='notification_message'>{$notification->message}</span> <span class='pull-right'><i class='fa {$icon} fa-fw'></i> {$date}</span>";
            $output .=     '</td>';
            $output .= '</tr>';
        }

        $data['data'] = $output;
        $data['total_notifications'] = $this->user_model->count_all_notifications();

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function load_notifications(): void
    {
        $this->load->helper('text');

        if ($this->input->post('seen') == 1) {
            $this->user_model->seen_notifications();
        }

        $user_type = $this->session->userdata('role_keyword') == ROLE_USER ? 'user' : 'admin';
        $data['unseen'] = $this->user_model->count_unseen_notifications();
        $all_notifications = $this->user_model->get_notifications($data['unseen']);
        $output = '';

        if (!empty($all_notifications)) {
            $output .= '<li>';
            $output .= '    <ul class="menu">';

            foreach ($all_notifications as $notification) {
                $message = wordwrap($notification->message, 35, '<br />');

                $time_different = floor(abs(strtotime(date('Y-m-d H:i:s')) - strtotime($notification->created_at)) / 60);

                $icon = 'fa-calendar';
                $date = $notification->created_at;
                if ($time_different < 10080) {
                    $icon = 'fa-clock-o';
                    $date = timespan(strtotime($notification->created_at), time(), 1) . ' ago';
                }

                $output .= '        <li>';
                $output .= '            <a href="' . ($notification->link == '#' ? $notification->link : site_url($notification->link)) . '">';
                $output .= "                <small>{$notification->title}<span class='pull-right'><i class='fa {$icon}'></i> {$date}</span></small><br />";
                $output .= "                {$message}";
                $output .= '            </a>';
                $output .= '        </li>';
            }

            $output .= '    </ul>';
            $output .= '</li>';

            $output .= '<li class="footer"><a href="' . site_url("{$user_type}/user/view_notification") . '">View all</a></li>';
        } else {
            $output = '<li><ul class="menu"><li><a href="#"><strong>No Notification Found</strong></a></li></ul></li>';
        }

        $data['notification'] = $output;


        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    } */
}
