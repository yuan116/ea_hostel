<?php

class Rooms extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('floors_model');
        $this->load->model('rooms_model');
    }

    public function list($floor_id): void
    {
        $template_data['tab_active'] = 1;
        $template_data['room_data'] = $this->floors_model->get_details($floor_id);

        if (empty($template_data['room_data'])) {
            $this->_redirect_404();
        }

        $tab_view['floor_id']      = $floor_id;
        $template_data['tab_view'] = $this->load->view('admin/rooms/rooms_list', $tab_view, TRUE);
        $data['content'] = $this->load->view('admin/rooms/rooms_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    public function get_list(int $floor_id): void
    {
        $data['data']         = $this->rooms_model->get_rooms_list($floor_id);
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function add_room(int $floor_id): void
    {
        $room_list = $this->rooms_model->get_rooms_list($floor_id);

        $insert_data = array(
            'floor_id' => $floor_id,
            'name'     => count($room_list) + 1,
        );

        $this->rooms_model->save($insert_data);

        redirect("admin/rooms/list/{$floor_id}");
    }

    public function change_status(): void
    {
        $this->rooms_model->save(array('status' => $this->input->post('status')), $this->input->post('room_id'));
    }
}
