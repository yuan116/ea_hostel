<?php

class Public_controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('announcements_model');
        $this->load->model('user_model');
        $this->load->model('blocks_model');
    }

    public function accommodation(): void
    {
        $data['data'] = $this->blocks_model->get_accommodation_details();

        $data['content'] = $this->load->view('public/accommodation', $data, TRUE);

        $this->load->view('html_layout', $data);
    }

    public function forgot_password(): void
    {
        if (!empty($_POST) && isset($_POST)) {
            if ($this->_forgot_password_form() === TRUE) {
                redirect();
            }
        }

        $announcement_data['announcements_list'] = $this->announcements_model->get_published_list();
        $template_data['announcement'] = $this->load->view('public/announcements_slider', $announcement_data, TRUE);
        $template_data['form']         = $this->load->view('public/forgot_password', NULL, TRUE);
        $data['content'] = $this->load->view('public/public_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _forgot_password_form(): bool
    {
        $result = FALSE;

        $this->form_validation->set_rules('username', 'Username', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $user_data = $this->user_model->get_user_details($this->input->post(), 'forgot_password');
            if (!empty($user_data)) {
                $result = $this->_send_forgot_password_email($user_data);

                if ($result == TRUE) {
                    $message = array(
                        'type'    => 'success',
                        'message' => 'Email has sent to your email address.'
                    );
                    $this->session->set_flashdata('message', $message);
                } else {
                    $message = array(
                        'type'    => 'error',
                        'message' => 'There is an error in our system.<br/>Please find Officer in Management Office for further assist.'
                    );
                    $this->session->set_flashdata('message', $message);
                }
            } else {
                $message = array(
                    'type'    => 'error',
                    'message' => 'Invalid username entered.'
                );
                $this->session->set_flashdata('message', $message);
            }
        }

        return $result;
    }

    private function _send_forgot_password_email($user_data): bool
    {
        $message = 'Hi {fullname},<br /><br />
                    It seem like you have forgot your password.<br />
                    Click below link to reset your password.<br />
                    <a href="{link}">EA Hostel Reset Password</a>';

        $message = str_replace('{fullname}', $user_data->fullname, $message);
        $message = str_replace('{link}', site_url("reset_password/{$user_data->user_id}/{$user_data->request_key}"), $message);

        $email_data['to']      = $user_data->email;
        $email_data['subject'] = 'Forgot Password';
        $email_data['message'] = $message;

        return $this->_send_mail($email_data);
    }

    public function login(): void
    {
        if (!empty($_POST) && isset($_POST)) {
            if ($this->_login_form() === TRUE) {
                $this->_redirect_loggedin();
            }
        }
        $announcement_data['announcements_list'] = $this->announcements_model->get_published_list();
        $template_data['announcement'] = $this->load->view('public/announcements_slider', $announcement_data, TRUE);
        $template_data['form']         = $this->load->view('public/login', NULL, TRUE);
        $data['content'] = $this->load->view('public/public_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _login_form(): bool
    {
        $result = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post();
            $data['password'] = $this->_get_hash_password($data['password']);
            $user_data = $this->user_model->get_user_details($data, 'login');

            if (!empty($user_data)) {
                $this->_set_login_session($user_data);

                $this->user_model->save_last_login($user_data->user_id);

                $result = TRUE;
            } else {
                $message = array(
                    'type'    => 'error',
                    'message' => 'Username or Password is not match.'
                );
                $this->session->set_flashdata('message', $message);
            }
        }

        return $result;
    }

    public function logout(): void
    {
        $this->session->sess_destroy();

        redirect();
    }

    public function reset_password(int $user_id, string $request_key): void
    {
        $where = array(
            'user_id'     => $user_id,
            'request_key' => $request_key,
        );

        $result = $this->user_model->check_data($where);

        if ($result === FALSE) {
            if (!empty($_POST) && isset($_POST)) {
                if ($this->_reset_password_form($user_id) === TRUE) {
                    $message = array(
                        'type'    => 'success',
                        'message' => 'Successfully reset password.'
                    );
                    $this->session->set_flashdata('message', $message);
                    redirect();
                }
            }

            $announcement_data['announcements_list'] = $this->announcements_model->get_published_list();
            $template_data['announcement'] = $this->load->view('public/announcements_slider', $announcement_data, TRUE);
            $template_data['form']         = $this->load->view('public/reset_password', NULL, TRUE);
            $data['content'] = $this->load->view('public/public_template', $template_data, TRUE);

            $this->load->view('html_layout', $data);
        } else {
            $this->_redirect_404();
        }
    }

    private function _reset_password_form(int $user_id): bool
    {
        $result = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[8]|max_length[50]'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'trim|required|min_length[8]|max_length[50]|matches[password]'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = array(
                'password'    => $this->_get_hash_password($this->input->post('password')),
                'request_key' => NULL,
            );

            $result = $this->user_model->save($data, $user_id);

            $this->_send_reset_password_email($user_id);
        }

        return $result;
    }

    private function _send_reset_password_email($user_id): bool
    {
        $user_data = $this->user_model->get_user_details(array('a.user_id' => $user_id));

        $message = 'Hi {fullname},<br /><br />
                    Your password has been reset.';

        $message = str_replace('{fullname}', $user_data->fullname, $message);

        $email_data['to']      = $user_data->email;
        $email_data['subject'] = 'Reset Password';
        $email_data['message'] = $message;

        return $this->_send_mail($email_data);
    }

    public function register(): void
    {
        if (!empty($_POST) && isset($_POST)) {
            if ($this->_register_form() === TRUE) {
                $message = array(
                    'type'    => 'success',
                    'message' => 'Register Successfully.'
                );
                $this->session->set_flashdata('message', $message);
                redirect();
            }
        }

        $data['content'] = $this->load->view('public/register', NULL, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _register_form(): bool
    {
        $result = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ),
            array(
                'field' => 'nric',
                'label' => 'Identity Card Number',
                'rules' => 'trim|integer|required|min_length[12]|max_length[12]'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|max_length[255]'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|min_length[10]|max_length[11]'
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|alpha_dash|max_length[50]|callback_check_username'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[8]|max_length[50]'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'trim|required|min_length[8]|max_length[50]|matches[password]'
            ),
            array(
                'field' => 'student_id',
                'label' => 'Student ID',
                'rules' => 'trim|required|max_length[10]'
            ),
            array(
                'field' => 'school_name',
                'label' => 'University/College Name',
                'rules' => 'trim|required|max_length[200]'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $user_data = $this->input->post(array('username', 'password', 'fullname', 'gender', 'nric', 'handphone_number', 'email'));
            $user_data['role_keyword'] = ROLE_USER;
            $user_data['password']     = $this->_get_hash_password($user_data['password']);

            $insert_id = $this->user_model->save($user_data);

            $student_data = $this->input->post(array('student_id', 'school_name'));
            $student_data['user_id'] = $insert_id;

            $this->user_model->save_student_details($student_data);

            $this->_send_welcome_email($user_data);

            $result = TRUE;
        }

        return $result;
    }

    private function _send_welcome_email(array $user_data): bool
    {
        $message = 'Hi {fullname},<br /><br />
                    Welcome to EA Hostel and Thanks for registering at our website.<br />
                    We hope our Hostel would provide you a suitable room according to your preference.';

        $message = str_replace('{fullname}', $user_data['fullname'], $message);

        $email_data['to']      = $user_data['email'];
        $email_data['subject'] = 'Welcome to EA Hostel';
        $email_data['message'] = $message;

        return $this->_send_mail($email_data);
    }
}
