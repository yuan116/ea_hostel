<?php

class Profile extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index()
    {
        $result = FALSE;

        if (!empty($_POST) && isset($_POST)) {
            if (isset($_POST['update_profile_btn'])) {
                $result = $this->_update_profile();
            } else if (isset($_POST['change_password_btn'])) {
                $result = $this->_change_password();
            } else if (isset($_POST['update_details_btn'])) {
                $result = $this->_update_student_details();
            }

            if ($result === TRUE) {
                $url = $this->session->userdata('role_keyword') == ROLE_USER ? 'user' : 'admin';
                redirect("{$url}/profile");
            }
        }

        $data['content'] = $this->load->view('public/form', NULL, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _change_password(): bool
    {
        $result = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'old_password',
                'label' => 'Old Password',
                'rules' => 'trim|required|callback_check_password'
            ),
            array(
                'field' => 'new_password',
                'label' => 'New Password',
                'rules' => 'trim|required|min_length[8]|max_length[50]'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'trim|required|min_length[8]|max_length[50]|matches[new_password]'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $result = $this->user_model->save(array('password' => $this->_get_hash_password($this->input->post('new_password'))), $this->input->post('user_id'));
        }

        return $result;
    }

    private function _update_profile(): bool
    {
        $result = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'trim|required|max_length[255]'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ),
            array(
                'field' => 'nric',
                'label' => 'Identity Card Number',
                'rules' => 'trim|integer|required|min_length[12]|max_length[12]'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|max_length[255]'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|min_length[10]|max_length[11]'
            ),
            array(
                'field' => 'personal_password_confirmation',
                'label' => 'Password Confirmation',
                'rules' => 'trim|required|callback_check_password'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $result = $this->user_model->save($this->input->post(array('fullname', 'gender', 'nric', 'email', 'handphone_number')), $this->input->post('user_id'));

            $user_data = $this->user_model->get_user_details(array('a.user_id' => $this->input->post('user_id')), 'login');
            $this->_set_login_session($user_data);
        }

        return $result;
    }

    private function _update_student_details()
    {
        $result = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'student_id',
                'label' => 'Student ID',
                'rules' => 'trim|required|max_length[10]'
            ),
            array(
                'field' => 'school_name',
                'label' => 'University/College Name',
                'rules' => 'trim|required|max_length[200]'
            ),
            array(
                'field' => 'student_password_confirmation',
                'label' => 'Password Confirmation',
                'rules' => 'trim|required|callback_check_password'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $result = $this->user_model->save_student_details($this->input->post(array('student_id', 'school_name')), $this->input->post('user_id'));

            $user_data = $this->user_model->get_user_details(array('a.user_id' => $this->input->post()), 'login');
            $this->_set_login_session($user_data);
        }

        return $result;
    }

    public function check_password(string $password): bool
    {
        $where = array(
            'user_id' => $this->session->userdata('user_id'),
            'password' => $this->_get_hash_password($password)
        );

        $result = $this->user_model->check_data($where);

        if ($result === FALSE) {
            $this->form_validation->set_message('check_password', 'Password is incorrect');
        }

        return $result;
    }
}
