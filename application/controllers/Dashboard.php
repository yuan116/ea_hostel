<?php

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('announcements_model');
    }

    public function index(): void
    {
        $dashboard['list'] = $this->announcements_model->get_dashboard_announcement_list();
        $data['content']   = $this->load->view('public/dashboard', $dashboard, TRUE);

        $this->load->view('html_layout', $data);
    }
}
