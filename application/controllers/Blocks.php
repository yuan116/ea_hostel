<?php

class Blocks extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('blocks_model');
        $this->load->model('floors_model');
    }

    public function floors_list(int $block_id): void
    {
        $template_data['tab_active'] = 1;
        $template_data['block_data'] = $this->blocks_model->get_details($block_id);

        if (empty($template_data['block_data'])) {
            $this->_redirect_404();
        }

        $tab_view['block_id']      = $block_id;
        $template_data['tab_view'] = $this->load->view('admin/blocks/floors_list', $tab_view, TRUE);
        $data['content'] = $this->load->view('admin/blocks/blocks_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    public function get_list(int $block_id): void
    {
        $data['data']         = $this->floors_model->get_floors_list($block_id);
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function add_floor(int $block_id): void
    {
        $floor_list = $this->floors_model->get_floors_list($block_id);

        $insert_data = array(
            'block_id' => $block_id,
            'name'     => count($floor_list) + 1,
        );

        $this->floors_model->save($insert_data);

        redirect("admin/blocks/floors_list/{$block_id}");
    }

    public function change_status(): void
    {
        $this->floors_model->save(array('status' => $this->input->post('status')), $this->input->post('floor_id'));
    }
}
