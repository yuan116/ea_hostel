<?php

class Feedbacks extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('feedbacks_model');
    }

    public function list(): void
    {
        $data['content']   = $this->load->view('feedbacks/feedbacks_list', NULL, TRUE);

        $this->load->view('html_layout', $data);
    }

    public function get_list(): void
    {
        $data['data']         = $this->feedbacks_model->get_list();
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function get_details()
    {
        $data = $this->feedbacks_model->get_details($this->input->post('feedback_id'));

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function form(): void
    {
        if (!empty($_POST) && isset($_POST)) {
            if ($this->_save_form() === TRUE) {
                redirect('user/dashboard');
            }
        }

        $data['content'] = $this->load->view('feedbacks/feedbacks_form', NULL, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _save_form(): bool
    {
        $result = FALSE;

        $data = $this->input->post(array('title', 'description'));
        $data['description'] = str_replace('<p><br></p>', '', $data['description']);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules(array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|required|max_length[100]'
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $this->feedbacks_model->save($data);

            /* $notification_data = array(
                'title'      => 'Feedback',
                'message'    => "{$this->session->userdata('fullname')} submitted a feedback.",
                'link'       => 'admin/feedbacks/list'
            );
            $this->_save_notification($notification_data, 'ALL_ADMIN'); */

            $result = TRUE;
        }

        return $result;
    }

    public function reply_feedback(int $feedback_id): void
    {
        if (!empty($_POST) && isset($_POST)) {
            if ($this->_save_reply_form($feedback_id) === TRUE) {
                redirect('admin/feedbacks/list');
            }
        }

        $content['data'] = $this->feedbacks_model->get_details($feedback_id);

        if (empty($content['data'])) {
            $this->_redirect_404();
        }

        $content['can_reply'] = TRUE;
        $data['content'] = $this->load->view('feedbacks/feedbacks_reply_form', $content, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _save_reply_form(int $feedback_id): bool
    {
        $result = FALSE;

        $data = $this->input->post(array('subject', 'message'));
        $data['message'] = str_replace('<p><br></p>', '', $data['message']);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules(array(
            array(
                'field' => 'subject',
                'label' => 'Subject',
                'rules' => 'trim|required|max_length[100]'
            ),
            array(
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'trim|required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $this->feedbacks_model->save(array('replied_by' => $this->session->userdata('user_id')), $feedback_id);

            $data['feedback_id'] = $feedback_id;

            $this->feedbacks_model->save_reply_feedbacks($data);

            /* $notification_data = array(
                'title'      => 'Reply Feedback',
                'message'    => "{$this->session->userdata('fullname')} reply your feedback. Please check your email.",
                'link'       => '#'
            );
            $this->_save_notification($notification_data, $this->input->post('user_id')); */
            $this->_send_reply_email($this->input->post());

            $result = TRUE;
        }

        return $result;
    }

    private function _send_reply_email(array $post_data): bool
    {
        $this->load->model('user_model');
        $user_data = $this->user_model->get_details($post_data['user_id']);

        $email_data['to']      = $user_data->email;
        $email_data['subject'] = $post_data['subject'];
        $email_data['message'] = $post_data['message'];

        return $this->_send_mail($email_data);
    }

    public function view(int $feedback_id): void
    {
        $content['data'] = $this->feedbacks_model->get_replied_feedback_details($feedback_id);

        if (empty($content['data'])) {
            $this->_redirect_404();
        }

        $content['can_reply'] = FALSE;
        $data['content'] = $this->load->view('feedbacks/feedbacks_reply_form', $content, TRUE);

        $this->load->view('html_layout', $data);
    }
}
