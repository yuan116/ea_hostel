<?php

class Request_room extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('floors_model');
        $this->load->model('rooms_model');
        $this->load->model('user_model');

        $this->rooms_model->set_property('table_name', 'tbl_request_room');
        $this->rooms_model->set_property('primary_key', 'request_id');
        $this->rooms_model->set_property('created_by', 'submitted_by');
        $this->rooms_model->set_property('created_date', 'submitted_at');
    }

    public function index()
    {
        $data['room_data'] = $this->rooms_model->get_user_room();

        if (!empty($data['room_data'])) {
            if ($data['room_data']->status == 'accepted') {
                $data['all_user'] = $this->rooms_model->get_room_users($data['room_data']->room_id);

                $all_name = $this->rooms_model->get_room_fullname($data['room_data']->room_id);
                $data['room_fullname'] = "{$all_name->building_name} {$all_name->block_name} - {$all_name->floor_name}" . str_pad($all_name->room_name, 2, 0, STR_PAD_LEFT);

                $data['content'] = $this->load->view('request_room/room_user', $data, TRUE);
            } else if ($data['room_data']->status == 'pending') {
                $data['content'] = $this->load->view('request_room/pending_view', $data, TRUE);
            }
        } else {
            if (!empty($_POST)) {
                if ($this->_save_request_form()) {
                    redirect('user/request_room');
                }
            }

            $data['building_list'] = $this->rooms_model->get_dropdown_list('a.building_id, a.name', 'a.building_id', 'a.name');
            $data['block_list'] = $this->rooms_model->get_dropdown_list('b.block_id, b.building_id, b.name, b.price', 'b.block_id', 'b.name');
            $data['floor_list'] = $this->rooms_model->get_dropdown_list('c.floor_id, c.block_id, c.name', 'c.floor_id,', 'c.name');
            $data['room_list'] = $this->rooms_model->get_dropdown_list('d.room_id, d.floor_id, d.name', 'd.room_id', 'd.name');

            $data['content'] = $this->load->view('request_room/form', $data, TRUE);
        }

        $this->load->view('html_layout', $data);
    }

    public function room(int $room_id)
    {
        $data['tab_active'] = 3;
        $data['all_user'] = $this->rooms_model->get_room_users($room_id);

        $all_name = $this->rooms_model->get_room_fullname($room_id);
        $data['room_fullname'] = "{$all_name->building_name} {$all_name->block_name} - {$all_name->floor_name}" . str_pad($all_name->room_name, 2, 0, STR_PAD_LEFT);

        $data['tab_view'] = $this->load->view('request_room/room_user', $data, TRUE);
        $data['content'] = $this->load->view('request_room/requested_template', $data, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _save_request_form()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'building_id',
                'label' => 'Building',
                'rules' => 'required'
            ),
            array(
                'field' => 'block_id',
                'label' => 'Block',
                'rules' => 'required'
            ),
            array(
                'field' => 'floor_id',
                'label' => 'Floor',
                'rules' => 'required'
            ),
            array(
                'field' => 'room_id',
                'label' => 'Room',
                'rules' => 'required'
            )
        ));

        if (empty($_FILES['file']['name'])) {
            $this->form_validation->set_rules('file', 'Payment Receipt', 'required');
        }

        if ($this->form_validation->run() === TRUE) {
            $data = array(
                'room_id' => $this->input->post('room_id'),
                'file' => $this->_get_upload_file(),
                'status' => 'pending'
            );

            return $this->rooms_model->save($data);
        }
    }

    private function _get_upload_file()
    {
        $config = array(
            'allowed_types' => 'jpeg|jpg|png|pdf',
            'file_name'     => "{$this->session->userdata('user_id')}_receipt",
        );

        return $this->_get_upload_file_path('file', $config);
    }

    public function list()
    {
        $data['tab_active'] = 1;

        $data['tab_view'] = $this->load->view('request_room/accepted_list', $data, TRUE);
        $data['content'] = $this->load->view('request_room/requested_template', $data, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function pending_list()
    {
        $data['tab_active'] = 2;

        $data['tab_view'] = $this->load->view('request_room/pending_list', $data, TRUE);
        $data['content'] = $this->load->view('request_room/requested_template', $data, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function get_list()
    {
        $data['data']         = $this->rooms_model->get_request_list($this->input->get('status'));
        $data['recordsTotal'] = sizeof($data['data']);

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function action(string $status, int $request_id)
    {
        if ($this->rooms_model->save(array('status' => $status), $request_id)) {
            if ($status == 'accepted') {
                $this->_send_accepted_mail($request_id);
            }

            redirect('admin/request_room/pending_list');
        }
    }

    private function _send_accepted_mail(int $request_id): bool
    {
        $user_data = $this->rooms_model->get_user_details($request_id);

        $email_data['to'] = $user_data->email;
        $email_data['subject'] = 'Request Room Accepted';
        $email_data['message'] = "Hi {$user_data->fullname}, <br />
                                  Your request has been accepted. <br />
                                  Kindly bring a hardcopy of your Identity Card and Student Card to management office for receive the key of the room.";

        return $this->_send_mail($email_data);
    }
}
