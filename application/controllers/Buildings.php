<?php

class Buildings extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('buildings_model');
        $this->load->model('blocks_model');
    }

    public function list(): void
    {
        $tab_view['buildings_list'] = $this->buildings_model->get_buildings_list();
        if (!empty($tab_view['buildings_list'])) {
            foreach ($tab_view['buildings_list'] as $value) {
                $tab_view['blocks_list'][$value->building_id] = $this->blocks_model->get_blocks_list($value->building_id);
            }
        }

        $template_data['tab_active'] = 1;
        $template_data['tab_view']   = $this->load->view('admin/buildings/buildings_list', $tab_view, TRUE);
        $data['content'] = $this->load->view('admin/buildings/buildings_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    public function building_form($building_id = NULL): void
    {
        if (!empty($_POST) && isset($_POST)) {
            if ($this->_save_building_form($building_id) === TRUE) {
                redirect('admin/buildings/list');
            }
        }

        $tab_view = array();
        if (!empty($building_id)) {
            $tab_view['data'] = $this->buildings_model->get_building_details($building_id);

            if (empty($tab_view['data'])) {
                $this->_redirect_404();
            }
        }

        $template_data['tab_active'] = 2;
        $template_data['tab_view']   = $this->load->view('admin/buildings/buildings_form', $tab_view, TRUE);
        $data['content'] = $this->load->view('admin/buildings/buildings_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _save_building_form($building_id): bool
    {
        $result = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'name',
                'label' => 'Building Name',
                'rules' => 'trim|required|max_length[10]|callback_check_building_name'
            ),
            array(
                'field' => 'type',
                'label' => 'Type',
                'rules' => 'required'
            ),
            array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post();

            if (empty($building_id)) {
                $this->buildings_model->save($data);
            } else {
                $this->buildings_model->save($data, $building_id);
            }

            $result = TRUE;
        }

        return $result;
    }

    public function check_building_name(): bool
    {
        $result = $this->buildings_model->check_data(array('name' => $this->input->post('name')));

        if ($result === FALSE) {
            $this->form_validation->set_message('check_building_name', 'This Building Name has been used by other buildings.');
        }

        return $result;
    }

    public function change_building_status(): void
    {
        $this->buildings_model->save(array('status' => $this->input->post('status')), $this->input->post('building_id'));
    }

    public function delete_building(int $building_id): void
    {
        if (empty($this->buildings_model->get_building_details($building_id))) {
            $this->_redirect_404();
        }

        $this->buildings_model->delete($building_id, TRUE);

        redirect('admin/buildings/list');
    }

    public function block_form(int $building_id, $block_id = NULL): void
    {
        if (empty($this->buildings_model->get_building_details($building_id))) {
            $this->_redirect_404();
        }

        if (!empty($_POST) && isset($_POST)) {
            if ($this->_save_block_form($building_id, $block_id) === TRUE) {
                redirect('admin/buildings/list');
            }
        }

        $tab_view = array();
        if (!empty($building_id) && !empty($block_id)) {
            $tab_view['data'] = $this->blocks_model->get_block_details($building_id, $block_id);

            if (empty($tab_view['data'])) {
                $this->_redirect_404();
            }
        }

        $tab_view['building_id']     = $building_id;
        $template_data['tab_active'] = 3;
        $template_data['tab_view']   = $this->load->view('admin/buildings/blocks_form', $tab_view, TRUE);
        $data['content'] = $this->load->view('admin/buildings/buildings_template', $template_data, TRUE);

        $this->load->view('html_layout', $data);
    }

    private function _save_block_form(int $building_id, $block_id): bool
    {
        $result = FALSE;

        $this->form_validation->set_rules(array(
            array(
                'field' => 'name',
                'label' => 'Block Name',
                'rules' => 'trim|required|max_length[10]|callback_check_block_name'
            ),
            array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|required|integer'
            ),
            array(
                'field' => 'number_of_person',
                'label' => 'Number of persons can live in one room',
                'rules' => 'trim|required|integer'
            ),
            array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post();

            if (empty($block_id)) {
                $data['building_id'] = $building_id;

                $this->blocks_model->save($data);
            } else {
                $this->blocks_model->save($data, $block_id);
            }

            $result = TRUE;
        }

        return $result;
    }

    public function check_block_name(): bool
    {
        $where = array(
            'building_id' => $this->input->post('building_id'),
            'name'        => $this->input->post('name')
        );

        $result = $this->blocks_model->check_data($where);

        if ($result === FALSE) {
            $this->form_validation->set_message('check_block_name', 'This Block Name has been used by other blocks.');
        }

        return $result;
    }

    public function change_block_status(): void
    {
        $this->blocks_model->save(array('status' => $this->input->post('status')), $this->input->post('block_id'));
    }

    public function delete_block(int $block_id): void
    {
        if (empty($this->blocks_model->get_block_details($block_id))) {
            $this->_redirect_404();
        }

        $this->blocks_model->delete($block_id, TRUE);

        redirect('admin/buildings/list');
    }
}
