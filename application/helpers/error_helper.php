<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('form_has_error')) {
    function form_has_error(string $field): string
    {
        $class = '';

        if (!empty(form_error($field))) {
            $class = 'has-error';
        }

        return $class;
    }
}

if (!function_exists('form_error_label')) {
    function form_error_label(string $field): string
    {
        $label = '';

        if (!empty(form_error($field))) {
            $label = '<label class="control-label" for="inputError">' . form_error($field) . '</label>';
        }

        return $label;
    }
}