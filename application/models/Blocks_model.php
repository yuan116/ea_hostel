<?php

class Blocks_model extends MY_Model
{
    protected $_table_name  = 'tbl_blocks';
    protected $_primary_key = 'block_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_details(int $block_id)
    {
        $this->db->select("CONCAT(a.name, ' - ', b.name) AS name, b.block_id");
        $this->db->from('tbl_buildings AS a');
        $this->db->join('tbl_blocks AS b', 'a.building_id = b.building_id');
        $this->db->where('b.block_id', $block_id);
        $this->db->where('a.deleted', 0);
        $this->db->where('b.deleted', 0);

        return $this->db->get()->row();
    }

    public function get_blocks_list(int $building_id)
    {
        $this->db->select('block_id, name, price, status');
        $this->db->from('tbl_blocks');
        $this->db->where('building_id', $building_id);
        $this->db->where('deleted', 0);
        $this->db->order_by('name');

        return $this->db->get()->result();
    }

    public function get_block_details(int $building_id, int $block_id)
    {
        $this->db->select('a.name as building_name, b.block_id, b.name, b.price, b.number_of_person, b.status');
        $this->db->from('tbl_buildings AS a');
        $this->db->from('tbl_blocks AS b', 'a.building_id = b.building_id');
        $this->db->where('a.building_id', $building_id);
        $this->db->where('b.block_id', $block_id);
        $this->db->where('a.deleted', 0);
        $this->db->where('b.deleted', 0);

        return $this->db->get()->row();
    }

    public function get_accommodation_details()
    {
        $this->db->select('MIN(price) AS min_price, MAX(price) AS max_price, MIN(number_of_person) AS min_person, MAX(number_of_person) AS max_person');
        $this->db->from('tbl_blocks');

        return $this->db->get()->row();
    }
}
