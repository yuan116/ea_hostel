<?php

class Rooms_model extends MY_Model
{
    protected $_table_name  = 'tbl_rooms';
    protected $_primary_key = 'room_id';

    public function get_rooms_list(int $floor_id)
    {
        $this->db->select('a.room_id, a.name, a.status, DATE_FORMAT(a.created_at, "' . CONVERT_DATETIME . '") AS created_at, b.fullname');
        $this->db->from('tbl_rooms AS a');
        $this->db->join('tbl_users AS b', 'a.created_by = b.user_id');
        $this->db->where('a.floor_id', $floor_id);
        $this->db->order_by('name');

        return $this->db->get()->result();
    }

    public function get_user_room()
    {
        $this->db->select('room_id, status');
        $this->db->from($this->_table_name);
        $this->db->where('submitted_by', $this->session->userdata('user_id'));
        $this->db->where_in('status', array('pending', 'accepted'));

        return $this->db->get()->row();
    }

    public function get_room_users(int $room_id)
    {
        $this->db->select('b.fullname, b.gender, b.nric, b.handphone_number, b.email, c.student_id, c.school_name');
        $this->db->from("{$this->_table_name} AS a");
        $this->db->join('tbl_users AS b', 'a.submitted_by = b.user_id');
        $this->db->join('tbl_student_details AS c', 'b.user_id = c.user_id');
        $this->db->where('a.room_id', $room_id);

        return $this->db->get()->result();
    }

    public function get_dropdown_list(string $select, string $group_by, string $order_by)
    {
        $requested_room = $this->_get_requested_room();

        $this->db->select($select);
        $this->db->from('tbl_buildings AS a');
        $this->db->join('tbl_blocks AS b', 'a.building_id = b.building_id');
        $this->db->join('tbl_floors AS c', 'b.block_id = c.block_id');
        $this->db->join('tbl_rooms AS d', 'c.floor_id = d.floor_id');
        $this->db->where('a.type', $this->session->userdata('gender'));
        $this->db->where('a.status', 1);
        $this->db->where('a.deleted', 0);
        $this->db->where('b.status', 1);
        $this->db->where('b.deleted', 0);
        $this->db->where('b.status', 1);
        $this->db->where('c.status', 1);
        $this->db->where('d.status', 1);

        if (!empty($requested_room)) {
            $this->db->where_not_in('d.room_id', $requested_room);
        }

        $this->db->group_by($group_by);
        $this->db->order_by($order_by);

        return $this->db->get()->result();
    }

    private function _get_requested_room()
    {
        $all_blocks = $this->db->select('block_id, number_of_person')->where('status', 1)->where('deleted', 0)->get('tbl_blocks')->result();

        $room_id = [];

        foreach ($all_blocks as $value) {
            $this->db->select('a.room_id');
            $this->db->from('tbl_request_room AS a');
            $this->db->join('tbl_rooms AS b', 'a.room_id = b.room_id');
            $this->db->join('tbl_floors AS c', 'b.floor_id = c.floor_id');
            $this->db->join('tbl_blocks AS d', ' c.block_id = d.block_id');
            $this->db->where_in('a.status', array('pending', 'accepted'));
            $this->db->where('b.status', 1);
            $this->db->where('c.status', 1);
            $this->db->where('d.status', 1);
            $this->db->where('d.deleted', 0);
            $this->db->where('d.block_id', $value->block_id);
            $this->db->group_by('a.room_id');
            $this->db->having('COUNT(*)', $value->number_of_person);
            $room = $this->db->get()->row();

            if (!empty($room)) {
                $room_id[] = $room->room_id;
            }
        }

        return $room_id;
    }

    public function get_request_list(string $status)
    {
        $this->db->select('a.request_id, a.room_id');
        $this->db->select('e.name AS building_name, d.name AS block_name, c.name AS floor_name, b.name AS room_name');
        $this->db->from('tbl_request_room AS a');
        $this->db->join('tbl_rooms AS b', 'a.room_id = b.room_id');
        $this->db->join('tbl_floors AS c', 'b.floor_id = c.floor_id');
        $this->db->join('tbl_blocks AS d', ' c.block_id = d.block_id');
        $this->db->join('tbl_buildings AS e', 'd.building_id = e.building_id');
        $this->db->where('a.status', $status);

        if ($status == 'accepted') {
            $this->db->select('COUNT(*) AS total_user');
            $this->db->group_by('a.room_id');
            $this->db->having('total_user > 0');
        } else if ($status == 'pending') {
            $this->db->select('f.user_id, f.fullname, a.file');
            $this->db->join('tbl_users AS f', 'a.submitted_by = f.user_id');
        }

        return $this->db->get()->result();
    }

    public function get_user_details(int $request_id)
    {
        $this->db->select('b.fullname, b.email');
        $this->db->from('tbl_request_room AS a');
        $this->db->join('tbl_users AS b', 'a.submitted_by = b.user_id');
        $this->db->where('a.request_id', $request_id);

        return $this->db->get()->row();
    }

    public function get_room_fullname(int $room_id)
    {
        $this->db->select('a.name AS building_name, b.name AS block_name, c.name AS floor_name, d.name AS room_name');
        $this->db->from('tbl_buildings AS a');
        $this->db->join('tbl_blocks AS b', 'a.building_id = b.building_id');
        $this->db->join('tbl_floors AS c', 'b.block_id = c.block_id');
        $this->db->join('tbl_rooms AS d', 'c.floor_id = d.floor_id');
        $this->db->where('d.room_id', $room_id);

        return $this->db->get()->row();
    }
}
