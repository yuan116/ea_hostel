<?php

class User_model extends MY_Model
{
    protected $_table_name  = 'tbl_users';
    protected $_primary_key = 'user_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_user_details(array $user_data, string $type = NULL)
    {
        $this->db->select('a.user_id, a.fullname, a.email');
        

        if ($type == 'login') {
            $this->db->select('a.role_keyword, a.username, a.gender, a.handphone_number, a.nric, b.student_id, b.school_name');
            $this->db->join('tbl_student_details AS b', 'a.user_id = b.user_id', 'left');
        } else if ($type == 'forgot_password') {
            $this->_set_request_key($user_data);
            $this->db->select('a.request_key');
        }

        $this->db->where($user_data);

        return $this->db->get('tbl_users AS a')->row();
    }

    private function _set_request_key($user_data): void
    {
        $this->load->helper('string');

        $data = array(
            'request_key' => random_string(),
            'updated_at'  => date('Y-m-d H:i:s'),
            'updated_by'  => NULL
        );

        $this->db->update($this->_table_name, $data, $user_data);
    }

    public function save_last_login(int $user_id): void
    {
        $this->save(array('last_login' => date('Y-m-d H:i:s')), $user_id);
    }

    public function get_list()
    {
        $this->db->select('a.user_id, a.username, a.fullname, a.handphone_number, a.email, DATE_FORMAT(a.created_at, "' . CONVERT_DATETIME . '") AS created_at, b.description');
        $this->db->select("CASE
            WHEN gender = 'M' THEN 'Male'
            ELSE 'Female'
        END AS gender");
        $this->db->from('tbl_users AS a');
        $this->db->join('tbl_roles AS b', 'a.role_keyword = b.keyword', 'left');
        $this->db->where_not_in('a.user_id', array(1, $this->session->userdata('user_id')));
        $this->db->where('a.deleted', 0);

        return $this->db->get()->result();
    }

    public function get_details(int $user_id)
    {
        $this->db->select('a.user_id, a.role_keyword, a.username, a.fullname, a.gender, a.nric, a.handphone_number, a.email, b.student_id, b.school_name');
        $this->db->from('tbl_users AS a');
        $this->db->join('tbl_student_details AS b', 'a.user_id = b.user_id', 'left');
        $this->db->where('a.user_id', $user_id);
        $this->db->where('a.deleted', 0);

        return $this->db->get()->row();
    }

    public function save_student_details(array $student_data, $user_id = NULL): bool
    {
        $this->_table_name = 'tbl_student_details';

        return $this->save($student_data, $user_id);
    }

    public function get_all_admin_list()
    {
        $this->db->select('user_id');
        $this->db->from('tbl_users');
        $this->db->where('role_keyword !=', ROLE_USER);
        $this->db->where('deleted', 0);

        return $this->db->get()->result();
    }

    public function get_request_user_details(int $user_id)
    {
        $this->db->select('a.fullname, a.nric, a.handphone_number, a.email, b.student_id, b.school_name');
        $this->db->select('CASE
                WHEN a.gender = "M" THEN "Male"
                ELSE "Female"
            END AS gender', FALSE);
        $this->db->from('tbl_users AS a');
        $this->db->join('tbl_student_details AS b', 'a.user_id = b.user_id');
        $this->db->where('a.user_id', $user_id);

        return $this->db->get()->row();
    }

    /* public function save_notification(array $data): bool
    {
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('user_id');

        $this->_table_name = 'tbl_notifications';
        return $this->save($data);
    }

    public function get_notifications($limit, $offset = 0)
    {
        $this->db->select('title, message, link, seen, created_at');
        $this->db->from('tbl_notifications');
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->order_by('created_at', 'desc');
        $this->db->limit($limit > 6 ? ceil($limit) : 6);
        $this->db->offset($offset);

        return $this->db->get()->result();
    }

    public function count_all_notifications()
    {
        $this->db->select('seen');
        $this->db->from('tbl_notifications');
        $this->db->where('user_id', $this->session->userdata('user_id'));
        return $this->db->get()->num_rows();
    }

    public function count_unseen_notifications(): int
    {
        $this->db->select('seen');
        $this->db->from('tbl_notifications');
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('seen', 0);
        return $this->db->get()->num_rows();
    }

    public function seen_notifications(): void
    {
        $update = array(
            'seen'    => 1,
            'seen_at' => date('Y-m-d H:i:s')
        );

        $where = array(
            'seen'    => 0,
            'user_id' => $this->session->userdata('user_id')
        );

        $this->db->update('tbl_notifications', $update, $where);
    } */
}
