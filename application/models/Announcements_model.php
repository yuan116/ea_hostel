<?php

class Announcements_model extends MY_Model
{
    protected $_table_name  = 'tbl_announcements';
    protected $_primary_key = 'announcement_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_published_list()
    {
        $this->db->select('title, description');
        $this->db->from('tbl_announcements');
        $this->db->where('status', 'published');
        $this->db->where('deleted', 0);
        $this->db->order_by('created_at');

        return $this->db->get()->result();
    }

    public function get_list()
    {
        $this->db->select('a.announcement_id, a.title, a.description, a.status, DATE_FORMAT(a.created_at, "' . CONVERT_DATETIME . '") AS created_at, b.fullname');
        $this->db->from('tbl_announcements AS a');
        $this->db->join('tbl_users AS b', 'a.created_by = b.user_id', 'left');
        $this->db->where('a.deleted', 0);
        $this->db->order_by('created_at', 'desc');

        return $this->db->get()->result();
    }

    public function get_details(int $announcement_id)
    {
        $this->db->select('announcement_id, title, description, status');
        $this->db->from('tbl_announcements');
        $this->db->where('announcement_id', $announcement_id);
        $this->db->where('deleted', 0);

        return $this->db->get()->row();
    }

    public function get_dashboard_announcement_list($offset = 0)
    {
        $this->db->select('a.announcement_id, a.title, a.description, DATE_FORMAT(a.created_at, "' . CONVERT_DATETIME_2 . '") AS created_at, b.fullname');
        $this->db->from('tbl_announcements AS a');
        $this->db->join('tbl_users AS b', 'a.created_by = b.user_id', 'left');
        $this->db->where('a.deleted', 0);
        $this->db->order_by('created_at', 'desc');
        $this->db->limit(20);

        return $this->db->get()->result();
    }
}
