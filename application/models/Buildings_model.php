<?php

class Buildings_model extends MY_Model
{
    protected $_table_name  = 'tbl_buildings';
    protected $_primary_key = 'building_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_buildings_list()
    {
        $this->db->select('building_id, name, type, status');
        $this->db->from('tbl_buildings');
        $this->db->where('deleted', 0);
        $this->db->order_by('name');

        return $this->db->get()->result();
    }

    public function get_building_details(int $building_id)
    {
        $this->db->select('building_id, name, type, status');
        $this->db->from('tbl_buildings');
        $this->db->where('building_id', $building_id);
        $this->db->where('deleted', 0);

        return $this->db->get()->row();
    }
}
