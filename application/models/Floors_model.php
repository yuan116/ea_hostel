<?php

class Floors_model extends MY_Model
{
    protected $_table_name  = 'tbl_floors';
    protected $_primary_key = 'floor_id';

    public function get_details(int $floor_id)
    {
        $this->db->select("CONCAT(a.name, ' - ', b.name, c.name) AS name, b.block_id, c.floor_id");
        $this->db->from('tbl_buildings AS a');
        $this->db->join('tbl_blocks AS b', 'a.building_id = b.building_id');
        $this->db->join('tbl_floors AS c', 'b.block_id = c.block_id');
        $this->db->where('c.floor_id', $floor_id);
        $this->db->where('a.deleted', 0);
        $this->db->where('b.deleted', 0);

        return $this->db->get()->row();
    }

    public function get_floors_list(int $block_id)
    {
        $this->db->select('a.floor_id, a.name, a.status, DATE_FORMAT(a.created_at, "' . CONVERT_DATETIME . '") AS created_at, b.fullname');
        $this->db->from('tbl_floors AS a');
        $this->db->join('tbl_users AS b', 'a.created_by = b.user_id');
        $this->db->where('a.block_id', $block_id);
        $this->db->order_by('name');

        return $this->db->get()->result();
    }
}