<?php

class Feedbacks_model extends MY_Model
{
    protected $_table_name  = 'tbl_feedbacks';
    protected $_primary_key = 'feedback_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_list()
    {
        $this->db->select('a.feedback_id, a.title, DATE_FORMAT(a.created_at, "' . CONVERT_DATETIME . '") AS submitted_at, b.fullname, c.fullname AS responder, DATE_FORMAT(d.created_at, "' . CONVERT_DATETIME . '") AS replied_at');
        $this->db->from("tbl_feedbacks AS a");
        $this->db->join('tbl_users AS b', 'a.created_by = b.user_id');
        $this->db->join('tbl_users AS c', 'a.replied_by = c.user_id', 'left');
        $this->db->join('tbl_reply_feedbacks AS d', 'a.feedback_id = d.feedback_id', 'left');
        $this->db->order_by('a.created_at', 'desc');

        return $this->db->get()->result();
    }

    public function get_details(int $feedback_id)
    {
        $this->db->select('a.title, a.description, a.created_by, a.created_at, b.fullname');
        $this->db->from('tbl_feedbacks AS a');
        $this->db->join('tbl_users AS b', 'a.created_by = b.user_id');
        $this->db->where('feedback_id', $feedback_id);

        return $this->db->get()->row();
    }

    public function save_reply_feedbacks(array $data): bool
    {
        $this->_table_name = 'tbl_reply_feedbacks';
        return $this->save($data);
    }

    public function get_replied_feedback_details(int $feedback_id)
    {
        $this->db->select('a.title, a.description, b.subject, b.message, DATE_FORMAT(b.created_at, "' . CONVERT_DATETIME . '") AS created_at, c.fullname');
        $this->db->from('tbl_feedbacks AS a');
        $this->db->join('tbl_reply_feedbacks AS b', 'a.feedback_id = b.feedback_id');
        $this->db->join('tbl_users AS c', 'b.created_by = c.user_id');
        $this->db->where('a.feedback_id', $feedback_id);

        return $this->db->get()->row();
    }
}
