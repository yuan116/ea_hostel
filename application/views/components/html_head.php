<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>EA Hostel</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap/dist/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/Ionicons/css/ionicons.min.css'); ?>">
    <!-- Checkbox Toggle -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/checkbox_toggle/bootstrap-toggle.min.css'); ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables.net/dataTables.bootstrap.min.css'); ?>">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/css/select2.min.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/AdminLTE.min.css'); ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/all-skins.min.css'); ?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css'); ?>">
    <!-- Summernote - text editor -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/summernote/summernote-lite.min.css'); ?>">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- jQuery 3 -->
    <script src="<?php echo base_url('assets/plugins/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url('assets/plugins/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url('assets/plugins/select2/js/select2.full.min.js'); ?>"></script>
    <!-- Checkbox Toggle -->
    <script src="<?php echo base_url('assets/plugins/checkbox_toggle/bootstrap-toggle.min.js'); ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url('assets/plugins/datatables.net/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datatables.net/dataTables.bootstrap.min.js'); ?>"></script>
    <!-- Daterangepicker -->
    <script src="<?php echo base_url('assets/plugins/moment/min/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
    <!-- Datepicker -->
    <script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
    <!-- Summetnote -->
    <script src="<?php echo base_url('assets/plugins/summernote/summernote-lite.min.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/dist/adminlte.min.js'); ?>"></script>

    <style>
        html, body {
            margin: 0;
            height: 100%;
        }

        div#background-image {
            background-image  : url(<?php echo base_url('assets/image/background.jpg'); ?>);
            background-repeat : no-repeat;
            background-size   : 100% 100%;
            height            : 100%;
            position          : absolute;
            width             : 100%;
        }

        footer.main-footer {
            position : relative;
            width    : 100%;
        }

        .has-error .select2-selection {
            border-color: rgb(185, 74, 72) !important;
        }

        .has-error .note-editor.note-frame {
            border-color: rgb(185, 74, 72) !important;
        }
    </style>

    <script>
    //Resolve conflict in jQuery UI tooltip with Bootstrap tooltip
    $.widget.bridge('uibutton', $.ui.button);

    $(document).ready(function(){
        //Select2
        $('.select2').select2({
            width             : '100%',
            dropdownAutoWidth : true
        });

        //Summernote
        $('.summernote').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['fontStyle', ['style', 'bold', 'italic', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript', 'fontname', 'fontsize']],
                ['color', ['forecolor', 'backcolor']],
                ['para', ['ul', 'ol', 'paragraph', 'height']],
                ['insert', ['picture', 'link', 'video', 'hr', 'table']],
                ['view', ['fullscreen', 'codeview','undo', 'redo', 'help']],
            ],
            height: 200
        });
    });
    </script>
</head>