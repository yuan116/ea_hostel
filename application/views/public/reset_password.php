<div class="box box-primary" style="height: 600px; overflow: auto;">
    <div class="box-body">
        <div class="login-logo">
            <a href="<?php echo site_url(); ?>"><b>EA Hostel</b></a>
        </div>
        <h3 class="text-center">Reset Password</h3>

        <form role="form" method="post" autocomplete="off">
            <div class="col-lg-12">
                <div class="form-group <?php echo form_has_error('password'); ?>">
                    <label class="control-label" for="password">Password</label>
                    <input type="password" class="form-control input-lg" name="password" id="password" min_length="8" maxlength="50" value="<?php echo set_value('password'); ?>" />
                    <?php echo form_error_label('password'); ?>

                </div>

                <div class="form-group <?php echo form_has_error('confirm_password'); ?>">
                    <label class="control-label" for="confirm_password">Confirm Password</label>
                    <input type="password" class="form-control input-lg" name="confirm_password" id="confirm_password" min_length="8" maxlength="50" value="<?php echo set_value('confirm_password'); ?>" />
                    <?php echo form_error_label('confirm_password'); ?>
                </div>

                <div class="form-group"><br />
                    <button type="submit" class="btn btn-lg btn-primary btn-block btn-flat">Submit</button><br />
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#confirm_password').keyup(function() {
            var password = $('#password').val();

            if (password == this.value) {
                $(this).parent('div.form-group').removeClass('has-error');
                $(this).next('label.control-label').html('');
                $('button[type="submit"]').prop('disabled', false);
            } else {
                $(this).parent('div.form-group').addClass('has-error');
                $(this).next('label.control-label').html('The Confirm Password field does not match the Password field.');
                $('button[type="submit"]').prop('disabled', true);
            }
        });
    });
</script>