<?php $data = $this->session->userdata(); ?>
<div class="box box-primary">
    <div class="box-body">
        <form role="form" method="post" autocomplete="off">
            <div class="col-lg-6">
                <div class="text-center">
                    <h4><strong>Personal Information</strong></h4>
                </div><br />

                <input type="hidden" name="user_id" id="user_id" value="<?php echo $data['user_id']; ?>" />

                <label class="col-lg-3 control-label" for="fullname">Full Name <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('fullname'); ?>">
                    <input type="text" class="form-control" name="fullname" id="fullname" maxlength="255" value="<?php echo set_value('fullname', $data['fullname']); ?>" />
                    <?php echo form_error_label('fullname'); ?>
                </div>

                <label class="col-lg-3 control-label" for="gender">Gender <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('gender'); ?>">
                    <select class="form-control select2" name="gender" id="gender">
                        <option value="">Please Select</option>
                        <option value="F" <?php echo set_select('gender', 'F', 'F' == $data['gender']); ?>>Female</option>
                        <option value="M" <?php echo set_select('gender', 'M', 'M' == $data['gender']); ?>>Male</option>
                    </select>
                    <?php echo form_error_label('gender'); ?>
                </div>

                <label class="col-lg-3 control-label" for="nric">Identity Card Number <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('nric'); ?>">
                    <input type="text" class="form-control" name="nric" id="nric" minlength="12" maxlength="12" value="<?php echo set_value('nric', $data['nric']); ?>" />
                    <p class="help-block">Please Enter Identity Card Number without '-'</p>
                    <?php echo form_error_label('nric'); ?>
                </div>

                <label class="col-lg-3 control-label" for="handphone_number">Handphone Number <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('handphone_number'); ?>">
                    <input type="text" class="form-control" name="handphone_number" id="handphone_number" minlength="10" maxlength="11" value="<?php echo set_value('handphone_number', $data['handphone_number']); ?>" />
                    <p class="help-block">Please Enter Handphone Number without '-'</p>
                    <?php echo form_error_label('handphone_number'); ?>
                </div>

                <label class="col-lg-3 control-label" for="email">Email <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('email'); ?>">
                    <input type="email" class="form-control" name="email" id="email" maxlength="255" value="<?php echo set_value('email', $data['email']); ?>" />
                    <?php echo form_error_label('email'); ?>
                </div>

                <label class="col-lg-3 control-label" for="personal_password_confirmation">Password Confirmation <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('personal_password_confirmation'); ?>">
                    <input type="password" class="form-control" name="personal_password_confirmation" id="personal_password_confirmation" value="<?php echo set_value('personal_password_confirmation'); ?>" />
                    <p class="help-block">Please enter password to update profile.</p>
                    <?php echo form_error_label('personal_password_confirmation'); ?>
                </div>

                <div class="col-lg-11">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-flat" name="update_profile_btn" id="update_profile_btn">Update Profile</button>
                    </div>
                </div>
            </div>
        </form>

        <form role="form" method="post" autocomplete="off">
            <div class="col-lg-6">
                <div class="text-center">
                    <h4><strong>Change Password</strong></h4>
                </div><br />

                <input type="hidden" name="user_id" id="user_id" value="<?php echo $data['user_id']; ?>" />

                <label class="col-lg-3 control-label" for="old_password">Old Password <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('old_password'); ?>">
                    <input type="password" class="form-control" name="old_password" id="old_password" maxlength="50" value="<?php echo set_value('old_password'); ?>" />
                    <?php echo form_error_label('old_password'); ?>
                </div>

                <label class="col-lg-3 control-label" for="new_password">New Password <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('new_password'); ?>">
                    <input type="password" class="form-control" name="new_password" id="new_password" min_length="8" maxlength="50" value="<?php echo set_value('new_password'); ?>" />
                    <?php echo form_error_label('new_password'); ?>
                </div>

                <label class="col-lg-3 control-label" for="confirm_password">Confirm Password <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('confirm_password'); ?>">
                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" min_length="8" maxlength="50" value="<?php echo set_value('confirm_password'); ?>" />
                    <?php echo form_error_label('confirm_password'); ?>
                </div>

                <div class="col-lg-11">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-flat" name="change_password_btn" id="change_password_btn">Change Password</button>
                    </div>
                </div>
            </div>
        </form>

        <?php if ($data['role_keyword'] == ROLE_USER) : ?>
            <form role="form" method="post" autocomplete="off">
                <div class="col-lg-6">
                    <div class="text-center">
                        <h4><strong>Student University/College Information</strong></h4>
                    </div><br />

                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $data['user_id']; ?>" />

                    <label class="col-lg-3 control-label" for="student_id">Student ID <span class="text-red">*</span></label>
                    <div class="col-lg-8 form-group <?php echo form_has_error('student_id'); ?>">
                        <input type="text" class="form-control" name="student_id" id="student_id" maxlength="20" value="<?php echo set_value('student_id', $data['student_id']); ?>" />
                        <?php echo form_error_label('student_id'); ?>
                    </div>

                    <label class="col-lg-3 control-label" for="school_name">University/College Name <span class="text-red">*</span></label>
                    <div class="col-lg-8 form-group <?php echo form_has_error('school_name'); ?>">
                        <input type="text" class="form-control" name="school_name" id="school_name" maxlength="200" value="<?php echo set_value('school_name', $data['school_name']); ?>" />
                        <?php echo form_error_label('school_name'); ?>
                    </div>

                    <label class="col-lg-3 control-label" for="student_password_confirmation">Password Confirmation <span class="text-red">*</span></label>
                    <div class="col-lg-8 form-group <?php echo form_has_error('student_password_confirmation'); ?>">
                        <input type="password" class="form-control" name="student_password_confirmation" id="student_password_confirmation" value="<?php echo set_value('student_password_confirmation'); ?>" />
                        <p class="help-block">Please enter password to update student University/College Information.</p>
                        <?php echo form_error_label('student_password_confirmation'); ?>
                    </div>

                    <div class="col-lg-11">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary btn-flat" name="update_details_btn" id="update_details_btn">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        <?php endif; ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#confirm_password').keyup(function() {
            var password = $('#new_password').val();

            if (password == this.value) {
                $(this).parent('div.form-group').removeClass('has-error');
                $(this).next('label.control-label').html('');
                $('button[type="submit"]#change_password_btn').prop('disabled', false);
            } else {
                $(this).parent('div.form-group').addClass('has-error');
                $(this).next('label.control-label').html('The Confirm Password field does not match the New Password field.');
                $('button[type="submit"]#change_password_btn').prop('disabled', true);
            }
        });

        $('#nric, #handphone_number').keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;

            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        });
    });
</script>