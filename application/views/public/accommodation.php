<style>
    .my_custom {
        background-color: #dd4b39;
        color: white;
        font-weight: bold;
    }
</style>

<div class="row">
    <div class="col-lg-6">
        <div class="box box-danger box-solid">
            <div class="box-header">
                <div class="box-title">
                    Hostel Information
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6">
                        <strong>Room facilities :</strong>
                        <ul>
                            <li>Beds with mattresses</li>
                            <li>Book shelves</li>
                            <li>Wardrobe</li>
                            <li>Curtain</li>
                            <li>Free Wi-Fi</li>
                            <li>Study table and chair</li>
                        </ul>
                    </div>

                    <div class="col-xs-6">
                        <strong>Hostel facilities :</strong>
                        <ul>
                            <li>Common room</li>
                            <li>Muslim prayer room</li>
                            <li>Pantry</li>
                            <li>Mini mart</li>
                            <li>Free Wi-Fi</li>
                            <li>Laundry room</li>
                        </ul>
                    </div>
                </div>

                <p>
                    <strong>Hostel Rental :</strong>
                    <ul>
                        <li><?php echo "{$data->min_person} - {$data->max_person} person per room."; ?></li>
                        <li><?php echo "RM{$data->min_price} - RM{$data->max_price} based on room."; ?></li>
                    </ul>
                </p>

                <p>
                    <strong>Application steps:</strong>
                    <ul>
                        <li>Login into system.</li>
                        <li>For the user that do not have an account, please register first.</li>
                        <li>Then go to room menu and choose the hostel room based on your prefernces.</li>
                        <li>Make payment via online and upload the payment receipt.</li>
                        <li>After submitted the request. Please wait for 3 working days for management to process the request.</li>
                        <li>Once request is accepted. You will receive an email with instructions.</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="box box-danger box-solid">
            <div class="box-header">
                <div class="box-title">
                    Contact Us
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <h4><strong>Location</strong></h4>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d913.4781009747637!2d101.64493737861338!3d2.925192390843103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cdb6e5ea5c4e1d%3A0xd990a3ae55a1e768!2sMMU%20Hostel%20HB1!5e0!3m2!1sen!2smy!4v1581473588435!5m2!1sen!2smy" width="100%" height="300" frameborder="0" style="border: 1px black solid;"></iframe>
                    </div>

                    <div class="col-xs-6">
                        <h4><strong>Office Number</strong></h4>
                        <h5>03-24083621</h5>
                    </div>

                    <div class="col-xs-6">
                        <h4><strong>Email</strong></h4>
                        <h5>ea_hostel@ea_hostel.com</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="box box-danger box-solid">
            <div class="box-header">
                <div class="box-title">
                    Image of hostel room
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-6">
                        <img src="<?php echo base_url('assets/image/double.jpg'); ?>" width="100%" height="300px" />
                    </div>
                    <div class="col-lg-6">
                        <img src="<?php echo base_url('assets/image/triple.jpg'); ?>" width="100%" height="300px" />
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>