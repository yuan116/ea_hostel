<?php $user_type = $this->session->userdata('role_keyword') == ROLE_USER ? 'user' : 'admin'; ?>

<style>
    .notification_title{
        font-weight: bold;
        font-size: 1.5em;
    }

    .notification_message{
        font-size: 1.25em;
    }
</style>

<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <div>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center notification_title">All Notifications</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div>
                <button type="button" class="btn btn-info btn-flat btn-block" onclick="load_notification();">Load Notifications</button>
            </div>
        </div>
    </div>
</div>

<script>
    var offset = 0;
    load_notification();

    function load_notification() {
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url("{$user_type}/user/load_notifications_list"); ?>',
            'data': {
                offset: offset,
            },
            'success': function(data) {
                offset += 20;
                $('tbody').append(data.data);

                if ((data.total_notifications - offset) <= 0) {
                    $('button').html('No More Notifications').removeClass('btn-info').addClass('btn-warning').prop('disabled', true);
                }
            }
        });
    }
</script>