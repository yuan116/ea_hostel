<div class="row">
    <div class="col-lg-6 col-lg-push-6">
        <?php echo $form; ?>
    </div>

    <div class="col-lg-6 col-lg-pull-6">
        <?php echo $announcement; ?>
    </div>
</div>