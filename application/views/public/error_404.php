<?php $return_to = $this->uri->rsegment(1) == 'public_controller' ? 'Login Page' : 'Dashboard'; ?>

<div class="row">
    <div class="box" style="height: 600px;">
        <div class="error-page" style="padding-top: 180px;">
            <h2 class="headline text-yellow"> 404</h2>

            <div class="error-content text-center" style="padding-top: 20px;">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

                <p class="text-center">
                    We could not find the page you were looking for.
                    Meanwhile, you may <a href="<?php echo site_url(); ?>">return to <?php echo $return_to; ?></a>.
                </p>
            </div>
        </div>
    </div>
</div>