<?php $nav_active = $this->uri->segment(1); ?>

<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a href="<?php echo site_url(); ?>" class="navbar-brand"><b>EA Hostel</b></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $nav_active == 'accommodation' ? 'active' : ''; ?>"><a href="<?php echo site_url('accommodation'); ?>">Accommodation</a></li>
                    <li class="<?php echo $nav_active == 'login' ? 'active' : ''; ?>"><a href="<?php echo site_url('login'); ?>">Login</a></li>
                    <li class="<?php echo $nav_active == 'register' ? 'active' : ''; ?>"><a href="<?php echo site_url('register'); ?>">Register</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>