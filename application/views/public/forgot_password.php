<div class="box box-primary" style="height: 600px; overflow: auto;">
    <div class="box-body">
        <div class="login-logo">
            <a href="<?php echo site_url(); ?>"><b>EA Hostel</b></a>
        </div>
        <h3 class="text-center">Forgot Password</h3>
        <?php
            $flash = $this->session->flashdata('message');
            if (!empty($flash)) :
        ?>
            <div class="alert alert-<?php echo $flash['type']; ?>"><?php echo $flash['message']; ?></div>
        <?php endif; ?>

        <form role="form" method="post" autocomplete="off">
            <div class="col-lg-12">
                <div class="form-group <?php echo form_has_error('username'); ?>">
                    <label class="control-label" for="username">Username</label>
                    <input type="text" class="form-control input-lg" name="username" id="username" placeholder="Username" value="<?php echo set_value('username'); ?>" />
                    <?php echo form_error_label('username'); ?>
                </div>

                <div class="form-group">
                    <p class="help-block">
                        Enter your username and click "Submit" button. We will send an email to your email address.
                    </p>
                </div>

                <div class="form-group"><br />
                    <button type="submit" class="btn btn-lg btn-primary btn-block btn-flat">Submit</button><br />
                    <a href="<?php echo site_url(); ?>" class="btn btn-lg btn-danger btn-block btn-flat">Remember Password</a>
                </div>
            </div>
        </form>
    </div>
</div>