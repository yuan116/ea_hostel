<div id="announcement_carousel" class="carousel slide" data-ride="carousel" style="background-color: rgba(255, 255, 255, 0.8); height: 600px;">
    <ol class="carousel-indicators">
        <?php
            if (!empty($announcements_list)) :
                foreach ($announcements_list as $key => $value) :
        ?>
                    <li data-target="#announcement_carousel" data-slide-to="<?php echo $key; ?>" class="<?php echo $key === 0 ? 'active' : '' ?>"></li>
        <?php
                endforeach;
            else :
        ?>
            <li data-target="#announcement_carousel" data-slide-to="0" class="active"></li>
        <?php endif; ?>
    </ol>

    <div class="carousel-inner">
        <?php
            if (!empty($announcements_list)) :
                foreach ($announcements_list as $key => $value) :
        ?>
                <div class="item <?php echo $key === 0 ? 'active' : '' ?>">
                    <div class="text-center">
                        <h3 style="color: black;"><?php echo $value->title; ?></h3>
                    </div>
                    <hr />
                    <div style="padding-left: 20px; height: 400px; overflow: auto; color: black;">
                        <?php echo $value->description; ?>
                    </div>
                </div>
        <?php
                endforeach;
            else :
        ?>
            <div class="item active">
                <div class="text-center">
                    <h3 style="color: black;">No Announcement</h3>
                </div>
                <hr />
                <div style="padding-left: 20px; height: 400px; overflow: auto; color: black;">
                    No Announcement
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>