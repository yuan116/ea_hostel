<div class="box box-primary" style="height: 600px; overflow: auto;">
    <div class="box-body">
        <div class="login-logo">
            <a href="<?php echo site_url(); ?>"><b>EA Hostel</b></a>
        </div>
        <h3 class="text-center">Login Form</h3>
        <?php
            $flash = $this->session->flashdata('message');
            if (!empty($flash)) :
        ?>
            <div class="alert alert-<?php echo $flash['type']; ?>"><?php echo $flash['message']; ?></div>
        <?php endif; ?>

        <form role="form" method="post" autocomplete="off">
            <div class="col-lg-12">
                <div class="form-group <?php echo form_has_error('username'); ?>">
                    <label class="control-label" for="username">Username</label>
                    <input type="text" class="form-control input-lg" name="username" id="username" placeholder="Username" value="<?php echo set_value('username'); ?>" />
                    <?php echo form_error_label('username'); ?>
                </div>

                <div class="form-group <?php echo form_has_error('password'); ?>">
                    <label class="control-label" for="password">Password</label>
                    <input type="password" class="form-control input-lg" name="password" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>" />
                    <?php echo form_error_label('password'); ?>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-lg btn-primary btn-block btn-flat">Login</button><br />
                    <a href="<?php echo site_url('register'); ?>" class="btn btn-lg btn-info btn-block btn-flat">Register New Account</a><br />
                    <a href="<?php echo site_url('forgot_password'); ?>" class="btn btn-lg btn-danger btn-block btn-flat">Forgot Password?</a>
                </div>
            </div>
        </form>
    </div>
</div>