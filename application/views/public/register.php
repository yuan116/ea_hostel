<div class="box box-primary">
    <div class="box-body">
        <div class="text-center">
            <h3><strong>Registration Form</strong></h3>
        </div>

        <form role="form" method="post" autocomplete="off">
            <div class="col-lg-6">
                <div class="text-center">
                    <h4><strong>Personal Information</strong></h4>
                </div><br />

                <label class="col-lg-3 control-label" for="fullname">Full Name <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('fullname'); ?>">
                    <input type="text" class="form-control" name="fullname" id="fullname" maxlength="255" value="<?php echo set_value('fullname'); ?>" />
                    <?php echo form_error_label('fullname'); ?>
                </div>

                <label class="col-lg-3 control-label" for="gender">Gender <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('gender'); ?>">
                    <select class="form-control select2" name="gender" id="gender">
                        <option value="">Please Select</option>
                        <option value="F" <?php echo set_select('gender', 'F'); ?>>Female</option>
                        <option value="M" <?php echo set_select('gender', 'M'); ?>>Male</option>
                    </select>
                    <?php echo form_error_label('gender'); ?>
                </div>

                <label class="col-lg-3 control-label" for="nric">Identity Card Number <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('nric'); ?>">
                    <input type="text" class="form-control" name="nric" id="nric" minlength="12" maxlength="12" value="<?php echo set_value('nric'); ?>" />
                    <p class="help-block">Please Enter Identity Card Number without '-'</p>
                    <?php echo form_error_label('nric'); ?>
                </div>

                <label class="col-lg-3 control-label" for="handphone_number">Handphone Number <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('handphone_number'); ?>">
                    <input type="text" class="form-control" name="handphone_number" id="handphone_number" minlength="10" maxlength="11" value="<?php echo set_value('handphone_number'); ?>" />
                    <p class="help-block">Please Enter Handphone Number without '-'</p>
                    <?php echo form_error_label('handphone_number'); ?>
                </div>

                <label class="col-lg-3 control-label" for="email">Email <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('email'); ?>">
                    <input type="email" class="form-control" name="email" id="email" maxlength="255" value="<?php echo set_value('email'); ?>" />
                    <?php echo form_error_label('email'); ?>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="text-center">
                    <h4><strong>Login Information</strong></h4>
                </div><br />

                <label class="col-lg-3 control-label" for="username">Username <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('username'); ?>">
                    <input type="text" class="form-control" name="username" id="username" maxlength="50" value="<?php echo set_value('username'); ?>" />
                    <?php echo form_error_label('username'); ?>
                </div>

                <label class="col-lg-3 control-label" for="password">Password <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('password'); ?>">
                    <input type="password" class="form-control" name="password" id="password" min_length="8" maxlength="50" value="<?php echo set_value('password'); ?>" />
                    <?php echo form_error_label('password'); ?>
                </div>

                <label class="col-lg-3 control-label" for="confirm_password">Confirm Password <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('confirm_password'); ?>">
                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" min_length="8" maxlength="50" value="<?php echo set_value('confirm_password'); ?>" />
                    <?php echo form_error_label('confirm_password'); ?>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="text-center">
                    <h4><strong>Student University/College Information</strong></h4>
                </div><br />

                <label class="col-lg-3 control-label" for="student_id">Student ID <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('student_id'); ?>">
                    <input type="text" class="form-control" name="student_id" id="student_id" maxlength="10" value="<?php echo set_value('student_id'); ?>" />
                    <?php echo form_error_label('student_id'); ?>
                </div>

                <label class="col-lg-3 control-label" for="school_name">University/College Name <span class="text-red">*</span></label>
                <div class="col-lg-8 form-group <?php echo form_has_error('school_name'); ?>">
                    <input type="text" class="form-control" name="school_name" id="school_name" maxlength="200" value="<?php echo set_value('school_name'); ?>" />
                    <?php echo form_error_label('school_name'); ?>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-flat">Register</button>
                    <a href="<?php echo site_url(); ?>" class="btn btn-info btn-flat">Have Account - Go To Login</a>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#confirm_password').keyup(function() {
            var password = $('#password').val();

            if (password == this.value) {
                $(this).parent('div.form-group').removeClass('has-error');
                $(this).next('label.control-label').html('');
                $('button[type="submit"]').prop('disabled', false);
            } else {
                $(this).parent('div.form-group').addClass('has-error');
                $(this).next('label.control-label').html('The Confirm Password field does not match the Password field.');
                $('button[type="submit"]').prop('disabled', true);
            }
        });

        $('#nric, #handphone_number').keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;

            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        });

        $('#username').keyup(function() {
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('check_username'); ?>',
                dataType: 'json',
                data: {
                    'username': this.value,
                    'type': 'ajax'
                },
                encode: true,
                success: function(data) {
                    $('#username').next('label').html(data.message);

                    if (data.result === true) {
                        $('#username').next('label').removeClass('text-danger');
                        $('button[type="submit"]').prop('disabled', false);
                    } else if (data.result === false) {
                        $('#username').next('label').addClass('text-danger');
                        $('button[type="submit"]').prop('disabled', true);
                    }
                }
            });
        });
    });
</script>