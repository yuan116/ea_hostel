<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <div class="row">
                <?php
                    if (!empty($list)) :
                        foreach ($list as $value) :
                ?>
                            <div class="col-lg-12">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="<?php echo "#{$value->announcement_id}"; ?>">
                                                <div class="row" style="padding-left: 15px; padding-right: 15px;">
                                                    <strong><?php echo $value->title; ?></strong>
                                                    <span class="pull-right">
                                                        <small><?php echo "{$value->fullname} - {$value->created_at}"; ?></small>
                                                    </span>
                                                </div>
                                            </a>
                                        </div>
                                        <div id="<?php echo $value->announcement_id; ?>" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <?php echo $value->description; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                <?php
                        endforeach;
                    else :
                ?>
                    <div class="col-lg-12">
                        <div class="alert alert-danger">
                            <strong>No Announcement Found</strong>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>