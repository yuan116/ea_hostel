<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <div class="row">
                <form role="form" method="post" autocomplete="off">
                    <div class="text-center">
                        <h3><strong>Reply Feedback Form</strong></h3>
                    </div><br />
                    <div class="col-lg-1"></div>
                    <div class="col-lg-11">
                        <div class="col-lg-12">
                            <label class="col-lg-<?php echo $can_reply === FALSE ? 1 : 2; ?> control-label">Feedback Title</label>
                            <div class="col-lg-<?php echo $can_reply === FALSE ? 9 : 8; ?> form-group">
                                <input type="text" class="form-control" maxlength="100" value="<?php echo $data->title; ?>" />
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-<?php echo $can_reply === FALSE ? 1 : 2; ?> control-label">Feedback Description</label>
                            <div class="col-lg-<?php echo $can_reply === FALSE ? 9 : 8; ?> form-group">
                                <div class="panel panel-default" style="height: 150px; overflow: auto; padding-left: 13px;">
                                    <?php echo $data->description; ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <?php if ($can_reply === TRUE) : ?>
                                <input type="hidden" name="user_id" id="user_id" value=<?php echo $data->created_by; ?>>
                            <?php endif ?>

                            <label class="col-lg-<?php echo $can_reply === FALSE ? 1 : 2; ?> control-label" for="subject">Subject <span class="text-red"><?php echo $can_reply === TRUE ? '*' : ''; ?></span></label>
                            <div class="col-lg-<?php echo $can_reply === FALSE ? 9 : 8; ?> form-group <?php echo form_has_error('subject'); ?>">
                                <input type="text" class="form-control" name="subject" id="subject" maxlength="100" value="<?php echo set_value('subject', @$data->subject); ?>" />
                                <?php echo form_error_label('subject'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-<?php echo $can_reply === FALSE ? 1 : 2; ?> control-label" for="message">Message <span class="text-red"><?php echo $can_reply === TRUE ? '*' : ''; ?></span></label>
                            <div class="col-lg-<?php echo $can_reply === FALSE ? 9 : 8; ?> form-group <?php echo form_has_error('message'); ?>">
                                <?php if ($can_reply === TRUE) : ?>
                                    <textarea class="form-control summernote" name="message" id="message"><?php echo set_value('message', @$data->message); ?></textarea>
                                    <?php echo form_error_label('message'); ?>
                                <?php else : ?>
                                    <div class="panel panel-default" style="height: 150px; overflow: auto; padding-left: 13px;">
                                        <?php echo $data->message; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <?php if ($can_reply === FALSE) : ?>
                            <div class="col-lg-12">
                                <label class="col-lg-1 control-label" for="message">Replied At </label>
                                <div class="col-lg-3 form-group">
                                    <input type="text" class="form-control" value="<?php echo $data->created_at; ?>" />
                                </div>
                                <label class="col-lg-1 control-label" for="message">Replied By </label>
                                <div class="col-lg-5 form-group">
                                    <input type="text" class="form-control" value="<?php echo $data->fullname; ?>" />
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="col-lg-12">
                            <div class="col-xs-2"></div>
                            <div class="col-lg-8 form-group">
                                <div class="pull-right">
                                    <?php if ($can_reply === TRUE) : ?>
                                        <button type="submit" class="btn btn-primary btn-flat">Send</button>
                                        <button type="button" class="btn btn-warning btn-flat" id="reset_btn">Reset</button>
                                    <?php endif; ?>
                                    <a href="<?php echo site_url('admin/feedbacks/list'); ?>" class="btn btn-danger btn-flat" onclick="window.location.href='<?php echo site_url('admin/feedbacks/list'); ?>';">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('input, textarea').prop('disabled', true);
        <?php if ($can_reply === TRUE) : ?>
            $('#user_id, #subject, #message').prop('disabled', false);
        <?php endif; ?>

        $('#reset_btn').click(function() {
            $('#subject').val('');
            $('#description').summernote('reset');
        });
    });
</script>