<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="feedbacks_dtb" width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Title</th>
                                    <th>Submitted By</th>
                                    <th>Submitted At</th>
                                    <th>Replied By</th>
                                    <th>Replied At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="feedback_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Feedback Details</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th>Title</th>
                        <td id="title"></td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td id="description"></td>
                    </tr>
                    <tr>
                        <th>Submitted At</th>
                        <td id="created_at"></td>
                    </tr>
                    <tr>
                        <th>Submitted By</th>
                        <td id="fullname"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    var feedbacks_dtb;
    $(document).ready(function() {
        feedbacks_dtb = $('#feedbacks_dtb').DataTable({
            "ajax": '<?php echo site_url('admin/feedbacks/get_list'); ?>',
            "columns": [
                {data : null},
                {data : 'title',        name: 'title'},
                {data : 'fullname',     name: 'fullname'},
                {data : 'submitted_at', name: 'submitted_at'},
                {data : 'responder',    name: 'responder'},
                {data : 'replied_at',   name: 'replied_at'},
                {data : null}
            ],
            "order": [
                [3, 'asc']
            ],
            "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                },

                {
                    'render': function(data, type, row) {
                        var btn = '';

                        btn += '<button type="button" class="btn btn-xs btn-default btn-flat" onclick="view_feedback(' + row['feedback_id'] + ');" title="View Feedback"><i class="fa fa-external-link fa-fw"></i></button>&nbsp;';

                        if (row['responder'] == null || row['responder'] == '') {
                            btn += '<a href="<?php echo site_url('admin/feedbacks/reply_feedback'); ?>' + '/' + row['feedback_id'] + '" class="btn btn-xs btn-info btn-flat" title="Reply Feedback"><i class="fa fa-envelope-o fa-fw"></i></a>&nbsp;';
                        }else{
                            btn += '<a href="<?php echo site_url('admin/feedbacks/view'); ?>' + '/' + row['feedback_id'] + '" class="btn btn-xs btn-primary btn-flat" title="View Reply"><i class="fa fa-eye fa-fw"></i></a>';
                        }

                        return btn;
                    },
                    "searchable": false,
                    "orderable": false,
                    "targets": 6
                },
            ]
        });

        feedbacks_dtb.on('order.dt search.dt', function() {
            feedbacks_dtb.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });

    function view_feedback(feedback_id) {
        $.ajax({
            'type': 'POST',
            'url': '<?php echo site_url('admin/feedbacks/get_details'); ?>',
            'data': {
                feedback_id: feedback_id,
            },
            'success': function(data) {
                $('#title').html(data.title);
                $('#description').html(data.description);
                $('#created_at').html(data.created_at);
                $('#fullname').html(data.fullname);
                $('#feedback_modal').modal('show');
            }
        });
    }
</script>