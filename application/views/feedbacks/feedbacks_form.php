<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <div class="row">
                <form role="form" method="post" autocomplete="off">
                    <div class="text-center">
                        <h3><strong>Feedback Form</strong></h3>
                    </div><br />
                    <div class="col-lg-1"></div>
                    <div class="col-lg-11">
                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="title">Title <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('title'); ?>">
                                <input type="text" class="form-control" name="title" id="title" maxlength="100" value="<?php echo set_value('title'); ?>" />
                                <?php echo form_error_label('title'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="description">Description <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('description'); ?>">
                                <textarea class="form-control summernote" name="description" id="description"><?php echo set_value('description'); ?></textarea>
                                <?php echo form_error_label('description'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="col-xs-2"></div>
                            <div class="col-lg-8 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                                    <button type="button" class="btn btn-danger btn-flat" id="reset_btn">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#reset_btn').click(function() {
            $('#title').val('');
            $('#description').summernote('reset');
        });
    });
</script>