<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                        <h2>Request Room Successfully</h2>
                        <h3>Status: Pending</h3>
                        <p class="help-block"><br />
                            <h4>
                                Thank you for apply a room with us. <br />
                                Please wait for 3 days for our management to process your request.
                            </h4>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>