<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="request_dtb" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Building</th>
                                <th>Block</th>
                                <th>Floor</th>
                                <th>Room</th>
                                <th>Full Name</th>
                                <th>Receipt</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="user_details_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">User Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-hovered">
                            <tr>
                                <th>Fullname</th>
                                <td id="fullname"></td>
                            </tr>
                            <tr>
                                <th>Gender</th>
                                <td id="gender"></td>
                            </tr>
                            <tr>
                                <th>Identity Card Number</th>
                                <td id="nric"></td>
                            </tr>
                            <tr>
                                <th>Handphone Number</th>
                                <td id="handphone_number"></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td id="email"></td>
                            </tr>
                            <tr>
                                <th>Student ID</th>
                                <td id="student_id"></td>
                            </tr>
                            <tr>
                                <th>School Name</th>
                                <td id="school_name"></td>
                            </tr>
                        </table>
                        <div class="pull-right">
                            <button type="button" class="btn btn-info btn-flat" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var request_dtb;
    $(document).ready(function() {
        request_dtb = $('#request_dtb').DataTable({
            "ajax"       : {
                url : '<?php echo site_url('admin/request_room/get_list'); ?>',
                data:{
                    'status' : 'pending',
                }
            },
            "columns"    : [
                {data : null},
                {data : 'building_name',    name : 'name'},
                {data : 'block_name',       name : 'block_name'},
                {data : 'floor_name',       name : 'floor_name'},
                {data : 'room_name',        name : 'room_name'},
                {data : null},
                {data : null}
            ],
            "order"      : [[0, 'asc']],
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : [1, 2, 3, 4]
                },
                {
                    'render': function(data, type, row){
                        var button = '<button class="btn btn-xs btn-link btn-flat" onclick="show_user_details(' + row['user_id'] + ')">' + row['fullname'] + '</button>';
                        
                        return button;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 5
                },
                {
                    'render': function(data, type, row){
                        var file = '<a href="<?php echo base_url(); ?>' + row['file'] + '" target="_blank" class="btn btn-xs btn-link">View</a>';
                        
                        return file;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 6
                },
                {
                    'render': function(data, type, row){
                        var button = '<a href="<?php echo site_url('admin/request_room/action/accepted/') ?>' + row['request_id'] + '" class="btn btn-xs btn-success btn-flat" onclick="return window.confirm(\'Are you want to accept?\');" title="Accept"><i class="fa fa-check"></i></a>&nbsp;';
                    
                        button += '<a href="<?php echo site_url('admin/request_room/action/rejected/') ?>' + row['request_id'] + '" class="btn btn-xs btn-danger btn-flat" onclick="return window.confirm(\'Are you want to reject?\');" title="Reject"><i class="fa fa-times"></i></a>'

                        return button;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 7
                }
            ]
        });

        request_dtb.on('order.dt search.dt', function () {
            request_dtb.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });

    function show_user_details(user_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('admin/user/get_request_user_details'); ?>',
            dataType: 'json',
            data: {
                'user_id': user_id,
            },
            encode: true,
            success: function(user_data) {
                $.each(user_data, function(key, value){
                    $('#' + key).html(value);
                });

                $('#user_details_modal').modal('show');
            }
        });
    }
</script>