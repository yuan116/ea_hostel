<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden;">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-center"><?php echo $room_fullname; ?></h3>
                    <?php foreach($all_user as $value): ?>
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>Full Name</th>
                                <th>Gender</th>
                                <th>Identity Card Number</th>
                                <th>Handphone Number</th>
                                <th>Email</th>
                                <th>Student ID</th>
                                <th>School Name</th>
                            </tr>
                            <tr>
                                <td><?php echo $value->fullname; ?></td>
                                <td><?php echo $value->gender == 'M' ? 'Male' : 'Female'; ?></td>
                                <td><?php echo $value->nric; ?></td>
                                <td><?php echo $value->handphone_number; ?></td>
                                <td><?php echo $value->email; ?></td>
                                <td><?php echo $value->student_id; ?></td>
                                <td><?php echo $value->school_name; ?></td>
                            </tr>
                        </table>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>