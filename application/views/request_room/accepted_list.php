<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="request_dtb" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Building</th>
                                <th>Block</th>
                                <th>Floor</th>
                                <th>Room</th>
                                <th>Number of Person</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var request_dtb;
    $(document).ready(function() {
        request_dtb = $('#request_dtb').DataTable({
            "ajax"       : {
                url : '<?php echo site_url('admin/request_room/get_list'); ?>',
                data:{
                    'status' : 'accepted',
                }
            },
            "columns"    : [
                {data : null},
                {data : 'building_name',    name : 'name'},
                {data : 'block_name',       name : 'block_name'},
                {data : 'floor_name',       name : 'floor_name'},
                {data : 'room_name',        name : 'room_name'},
                {data : 'total_user',       name : 'total_user'},
            ],
            "order"      : [[0, 'asc']],
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : [1, 2, 3, 4]
                },
                {
                    'render': function(data, type, row){
                        var button = '<a href="<?php echo site_url('admin/request_room/room/') ?>' + row['room_id'] + '" class="btn btn-xs btn-info btn-flat" title="View"><i class="fa fa-eye"></i></a>';
                    
                        return button;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 6
                }
            ]
        });

        request_dtb.on('order.dt search.dt', function () {
            request_dtb.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
</script>