<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <div class="row">
                <form role="form" method="post" autocomplete="off" enctype="multipart/form-data">
                    <div class="text-center">
                        <h3><strong>Request Room Form</strong></h3>
                    </div><br />
                    <div class="col-lg-1"></div>
                    <div class="col-lg-11">
                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="building_id">Building <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('building_id'); ?>">
                                <select class="form-control select2" name="building_id" id="building_id">
                                    <option value="">Please Select</option>
                                    <?php foreach ($building_list as $value) : ?>
                                        <option value="<?php echo $value->building_id; ?>" <?php echo set_select('building_id', $value->building_id); ?>><?php echo $value->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error_label('building_id'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="block_id">Block <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('block_id'); ?>">
                                <select class="form-control select2" name="block_id" id="block_id">
                                    <option value="">Please Select</option>
                                </select>
                                <?php echo form_error_label('block_id'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="floor_id">Floor <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('floor_id'); ?>">
                                <select class="form-control select2" name="floor_id" id="floor_id">
                                    <option value="">Please Select</option>
                                </select>
                                <?php echo form_error_label('floor_id'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="room_id">Room <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('room_id'); ?>">
                                <select class="form-control select2" name="room_id" id="room_id">
                                    <option value="">Please Select</option>
                                </select>
                                <?php echo form_error_label('room_id'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="price">Price (RM)</label>
                            <div class="col-lg-8 form-group">
                                <input type="text" class="form-control" name="price" id="price" value="0.00" readonly />
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <p class="col-lg-6 help-block">
                                Please make payment to below details: <br />
                                Bank Account : 012345679 <br />
                                Bank Account Name : EA HOSTEL <br />
                                Bank : CIMB Bank <br />
                            </p>
                            <div class="col-lg-6">
                                <p class="help-block">
                                    Click on the icon below will redirect you to the website.
                                </p>
                                <a href="https://www.ambank.com.my/eng/" target="_blank" title="AmBank"><img src="<?php echo base_url('assets/image/ambank.png'); ?>" alt="AmBank" width="30px"></a>
                                <a href="https://www.cimbclicks.com.my/" target="_blank" title="CIMB Bank"><img src="<?php echo base_url('assets/image/cimb.png'); ?>" alt="CIMB Bank" width="30px"></a>
                                <a href="https://www.hlb.com.my/en/personal-banking/home.html" target="_blank" title="Hong Leong Bank"><img src="<?php echo base_url('assets/image/hong_leong.png'); ?>" alt="Hong Leong Bank" width="30px"></a>
                                <a href="https://www.hsbc.com.my/" target="_blank" title="HSBC Bank"><img src="<?php echo base_url('assets/image/hsbc.png'); ?>" alt="HSBC Bank" width="30px"></a>
                                <a href="https://www.pbebank.com/" target="_blank" title="Public Bank"><img src="<?php echo base_url('assets/image/public.png'); ?>" alt="Public Bank" width="30px"></a>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="file">Payment Receipt<span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('file'); ?>">
                                <input type="file" name="file" id="file" accept="image/*, .pdf" />
                                <?php echo form_error_label('file'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="col-xs-2"></div>
                            <div class="col-lg-8 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var block_list = <?php echo json_encode($block_list); ?>;
    var floor_list = <?php echo json_encode($floor_list); ?>;
    var room_list = <?php echo json_encode($room_list); ?>;

    $(document).ready(function() {
        $('#building_id').change(function() {
            change_block();
        });

        $('#block_id').change(function(){
            change_floor();
        });

        $('#floor_id').change(function(){
            change_room();
        });

        function change_block() {
            var building_id = $('#building_id').val();

            if (building_id != '' || building_id != null) {
                $('#block_id').empty('').append('<option value="">Please Select</option>');

                $.each(block_list, function(key, value) {
                    if (value.building_id == building_id) {
                        $('#block_id').append('<option value="' + value.block_id + '" price="' + value.price + '">' + value.name + '</option>');
                    }
                });

                $('#block_id').val('<?php echo set_value('block_id'); ?>').change();
            }
        }

        function change_floor() {
            var block_id = $('#block_id').val();

            if (block_id != '' || block_id != null) {
                $('#floor_id').empty('').append('<option value="">Please Select</option>');

                $.each(floor_list, function(key, value) {
                    if (value.block_id == block_id) {
                        $('#floor_id').append('<option value="' + value.floor_id + '">' + value.name + '</option>');
                    }
                });

                $.each(block_list, function(key, value){
                    if (value.block_id == block_id) {
                        $('#price').val(value.price);
                    }
                });

                $('#floor_id').val('<?php echo set_value('floor_id'); ?>').change();
            }
        }

        function change_room() {
            var floor_id = $('#floor_id').val();

            if (floor_id != '' || floor_id != null) {
                $('#room_id').empty('').append('<option value="">Please Select</option>');

                $.each(room_list, function(key, value) {
                    if (value.floor_id == floor_id) {
                        $('#room_id').append('<option value="' + value.room_id + '">' + value.name + '</option>');
                    }
                });

                $('#room_id').val('<?php echo set_value('room_id'); ?>').change();
            }
        }

        change_block();
        change_floor();
        change_room();
    });
</script>