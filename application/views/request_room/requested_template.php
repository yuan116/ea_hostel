<div class="row">
    <div class="box box-default" style="height: 600px; overflow: hidden;">
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li class="<?php echo $tab_active == 1 ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/request_room/list'); ?>">Accepted Request List</a></li>
                <li class="<?php echo $tab_active == 2 ? 'active' : ''; ?>"><a href="<?php echo site_url("admin/request_room/pending_list") ?>">Pending Request List</a></li>
                <?php if($tab_active == 3): ?>
                    <li class="<?php echo $tab_active == 3 ? 'active' : ''; ?>"><a href="#"><strong><?php echo $room_fullname; ?></strong></a></li>
                <?php endif; ?>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <?php echo $tab_view; ?>
                </div>
            </div>
        </div>
    </div>
</div>