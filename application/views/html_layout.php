<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('components/html_head'); ?>

<body class="hold-transition skin-purple layout-top-nav">
    <div class="wrapper">
        <div id="background-image"></div>

        <?php
            $role_keyword = $this->session->userdata('role_keyword');
            $navigation = 'public';

            if ($role_keyword == ROLE_ADMINISTRATOR or $role_keyword == ROLE_USER_ADMIN) {
                $navigation = 'admin';
            } else if ($role_keyword == ROLE_USER) {
                $navigation = 'user';
            }

            $this->load->view("{$navigation}/{$navigation}_navigation");
        ?>

        <div class="content-wrapper">
            <div class="container">
                <section class="content container-fluid">
                    <?php echo $content; ?>
                </section>
            </div>
        </div>

        <footer class="main-footer">
            <div class="container">
                <div class="pull-right">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; <?php echo date('Y'); ?> EA Hostel System.</strong> All rights reserved.
            </div>
        </footer>
    </div>
</body>

</html>