<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <form role="form" method="post" autocomplete="off">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-11">
                        <input type="hidden" name="announcement_id" id="announcement_id" value="<?php echo @$data->announcement_id; ?>" />

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="title">Title <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('title'); ?>">
                                <input type="text" class="form-control" name="title" id="title" maxlength="100" value="<?php echo set_value('title', @$data->title); ?>" />
                                <?php echo form_error_label('title'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="description">Description <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('description'); ?>">
                                <textarea class="form-control summernote" name="description" id="description"><?php echo set_value('description', @$data->description); ?></textarea>
                                <?php echo form_error_label('description'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="title">Status <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('status'); ?>">
                                <select class="form-control select2" name="status" id="status">
                                    <option value="">Please Select</option>
                                    <option value="published" <?php echo set_select('status', 'published', 'published' == @$data->status); ?>>Published</option>
                                    <option value="unpublished" <?php echo set_select('status', 'unpublished', 'unpublished' == @$data->status); ?>>Unpublished</option>
                                </select>
                                <?php echo form_error_label('status'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="col-xs-2"></div>
                            <div class="col-lg-8 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    <?php if (empty($data)) : ?>
                                        <button type="button" class="btn btn-danger btn-flat" id="reset_btn">Reset</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#reset_btn').click(function() {
            $('#title').val('');
            $('#description').summernote('reset');
            $('#status').val('').trigger('change');
        });
    });
</script>