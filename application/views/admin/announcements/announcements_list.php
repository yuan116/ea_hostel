<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="announcements_dtb" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var announcements_dtb;
    $(document).ready(function() {
        announcements_dtb = $('#announcements_dtb').DataTable({
            "ajax"       : '<?php echo site_url('admin/announcements/get_list'); ?>',
            "columns"    : [
                {data : null},
                {data : 'title',      name : 'title'},
                {data : 'status',     name : 'status'},
                {data : 'fullname',   name : 'fullname'},
                {data : 'created_at', name : 'created_at'},
                {data : null}
            ],
            "order"      : [[4, 'asc']],
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 0
                },

                {
                    'render': function(data, type, row){
                        var label = '';

                        label += '<div class="change_status">';
                        label += 		'<input type="checkbox" announcement_id="' + row['announcement_id'] + '" value="1" data-toggle="toggle" ';
                                    
                        if(data == "published"){
                            label +=			'checked ';
                        }

                        label +=		'data-on="Published" data-off="Unpublished" data-onstyle="success btn-xs" data-offstyle="danger btn-xs"/>';
                        label += '</div>';

                        return label;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 2
                },

                {
                    'render': function(data, type, row) {
                        var btn = '';

                        btn += '<a href="<?php echo site_url('admin/announcements/form'); ?>' + '/' + row['announcement_id'] + '" class="btn btn-xs btn-info btn-flat" title="Edit"><i class="fa fa-edit fa-fw"></i></a>&nbsp;';
                        btn += '<a href="<?php echo site_url('admin/announcements/delete'); ?>' + '/' + row['announcement_id'] + '" class="btn btn-xs btn-danger btn-flat" title="Delete" onclick="return delete_announcement();"><i class="fa fa-trash-o fa-fw"></i></a>';

                        return btn;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 5
                },
            ]
        });

        announcements_dtb.on('order.dt search.dt', function () {
            announcements_dtb.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1;
            });

            $('.change_status input[type="checkbox"]').bootstrapToggle({
                size  : 'small',
                width : '100'
            });
        }).draw();
    });

    $('table').on({change: function(e) {
        var announcement_id = $(this).attr('announcement_id');
		var status = $(this).is(":checked") === true ? 'published' : 'unpublished';
        
		$.ajax({
			type : 'POST',
			url  : '<?php echo site_url('admin/announcements/change_status'); ?>',
            data : {
                "announcement_id" : announcement_id,
                "status"          : status,
            },
        });
	}}, 'input[type="checkbox"]');

    function delete_announcement() {
        return window.confirm('Are you you want to delete?\nOnce delete this record cannot be recover.');
    }
</script>