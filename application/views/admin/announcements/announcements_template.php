<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li class="<?php echo $tab_active == 1 ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/announcements/list'); ?>">Announcements List</a></li>
                <li class="<?php echo $tab_active == 2 ? 'active' : ''; ?>"><a href="<?php echo !empty($data) ? '#' : site_url('admin/announcements/form'); ?>"><?php echo !empty($data) ? $data->title : 'New Announcement'; ?></a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <?php echo $tab_view; ?>
                </div>
            </div>
        </div>
    </div>
</div>