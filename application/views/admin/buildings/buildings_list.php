<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <?php
                        if (!empty($buildings_list)) :
                            foreach ($buildings_list as $value) :
                    ?>
                                <div class="col-lg-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="<?php echo "#{$value->building_id}"; ?>">
                                                    <?php 
                                                        $gender = ($value->type == 'M') ? 'Male' : 'Female';
                                                        echo "Building Name: <strong>{$value->name} - {$gender}</strong>";
                                                    ?>
                                                </a>&nbsp;
                                                <input type="checkbox" class="change_building_status" name="status" building_id="<?php echo $value->building_id; ?>" <?php echo 1 == $value->status ? 'checked' : ''; ?> value="1" />
                                                <span class="pull-right">
                                                    <a href="<?php echo site_url("admin/buildings/block_form/{$value->building_id}"); ?>" class="btn btn-xs btn-primary btn-flat"><i class="fa fa-plus fa-fw"></i> New Block &nbsp;</a>
                                                    <a href="<?php echo site_url("admin/buildings/building_form/{$value->building_id}"); ?>" class="btn btn-xs btn-info btn-flat" title="Edit"><i class="fa fa-edit fa-fw"></i></a>
                                                    <a href="<?php echo site_url("admin/buildings/delete_building/{$value->building_id}"); ?>" class="btn btn-xs btn-danger btn-flat" title="Delete" onclick="return delete_building();"><i class="fa fa-trash-o fa-fw"></i></a>
                                                </span>
                                            </div>
                                            <div id="<?php echo $value->building_id; ?>" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <table width="100%" class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Block Name</th>
                                                                <th>Price (RM)</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                if (!empty($blocks_list[$value->building_id])) :
                                                                    foreach ($blocks_list[$value->building_id] as $block_value) :
                                                            ?>
                                                                        <tr>
                                                                            <td><a href="<?php echo site_url("admin/blocks/floors_list/{$block_value->block_id}") ?>"><?php echo "{$value->name} - {$block_value->name}"; ?></a></td>
                                                                            <td><?php echo $block_value->price; ?></td>
                                                                            <td>
                                                                                <input type="checkbox" class="change_block_status" block_id="<?php echo $block_value->block_id; ?>" <?php echo 1 == $block_value->status ? 'checked' : ''; ?> value="1" />
                                                                            </td>
                                                                            <td>
                                                                                <a href="<?php echo site_url("admin/buildings/block_form/{$value->building_id}/{$block_value->block_id}"); ?>" class="btn btn-xs btn-info btn-flat" title="Edit"><i class="fa fa-edit fa-fw"></i></a>&nbsp;
                                                                                <a href="<?php echo site_url("admin/buildings/delete_block/{$block_value->block_id}"); ?>" class="btn btn-xs btn-danger btn-flat" title="Delete" onclick="return delete_block();"><i class="fa fa-trash-o fa-fw"></i></a>
                                                                            </td>
                                                                        </tr>
                                                            <?php
                                                                    endforeach;
                                                                else :
                                                            ?>
                                                                    <tr>
                                                                        <th class="text-center" colspan="4">No Block Found</th>
                                                                    </tr>
                                                        <?php   endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    <?php
                            endforeach;
                        else :
                    ?>
                        <div class="col-lg-12">
                            <div class="alert alert-danger">
                                <strong>No Building Found</strong>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('input[type="checkbox"].change_building_status, input[type="checkbox"].change_block_status').bootstrapToggle({
            on      : 'Active',
            off     : 'Inactive',
            onstyle : 'success',
            offstyle: 'danger',
            size    : 'mini',
            width   : '50'
        });

        $('input[type="checkbox"].change_building_status').change(function() {
            var building_id = $(this).attr('building_id');
            var status = $(this).is(":checked") === true ? 1 : 0;

            $.ajax({
                type : 'POST',
                url  : '<?php echo site_url('admin/buildings/change_building_status'); ?>',
                data : {
                    "building_id" : building_id,
                    "status"      : status,
                },
            });
        });

        $('input[type="checkbox"].change_block_status').change(function() {
            var block_id = $(this).attr('block_id');
            var status = $(this).is(":checked") === true ? 1 : 0;

            $.ajax({
                type : 'POST',
                url  : '<?php echo site_url('admin/buildings/change_block_status'); ?>',
                data : {
                    "block_id" : block_id,
                    "status"   : status,
                },
            });
        });
    });

    function delete_building() {
        return window.confirm('Are you you want to delete?\nOnce delete this record cannot be recover.');
    }

    function delete_block() {
        return window.confirm('Are you you want to delete?\nOnce delete this record cannot be recover.');
    }
</script>