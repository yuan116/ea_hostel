<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li class="<?php echo $tab_active == 1 ? 'active' : ''; ?>"><a href="<?php echo site_url('admin/buildings/list'); ?>">Buildings List</a></li>
                <li class="<?php echo $tab_active == 2 ? 'active' : ''; ?>"><a href="<?php echo !empty($data) && $tab_active == 2 ? '#' : site_url('admin/buildings/building_form'); ?>"><?php echo !empty($data) && $tab_active == 2 ? $data->name : 'New Building'; ?></a></li>
                <?php if ($tab_active == 3) : ?>
                    <li class="<?php echo $tab_active == 3 ? 'active' : ''; ?>"><a href="#"><?php echo empty($data) ? 'New Block' : " {$data->building_name} - {$data->name}"; ?></a></li>
                <?php endif; ?>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <?php echo $tab_view; ?>
                </div>
            </div>
        </div>
    </div>
</div>