<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <form role="form" method="post" autocomplete="off">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-11">
                        <input type="hidden" name="building_id" id="building_id" value="<?php echo @$data->building_id; ?>" />

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="name">Building Name <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('name'); ?>">
                                <input type="text" class="form-control" name="name" id="name" maxlength="10" value="<?php echo set_value('name', @$data->name); ?>" />
                                <?php echo form_error_label('name'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="type">Type <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('type'); ?>">
                                <select class="form-control select2" name="type" id="type">
                                    <option value="">Please Select</option>
                                    <option value="F" <?php echo set_select('type', 'F', 'F' == @$data->type); ?>>Female</option>
                                    <option value="M" <?php echo set_select('type', 'M', 'M' == @$data->type); ?>>Male</option>
                                </select>
                                <?php echo form_error_label('type'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="status">Status <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('status'); ?>">
                                <select class="form-control select2" name="status" id="status">
                                    <option value="">Please Select</option>
                                    <option value="1" <?php echo set_select('status', '1', '1' === @$data->status); ?>>Active</option>
                                    <option value="0" <?php echo set_select('status', '0', '0' === @$data->status); ?>>Inactive</option>
                                </select>
                                <?php echo form_error_label('status'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="col-xs-2"></div>
                            <div class="col-lg-8 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    <?php if (empty($data)) : ?>
                                        <button type="button" class="btn btn-danger btn-flat" id="reset_btn">Reset</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#reset_btn').click(function() {
            $('#name').val('');
            $('#type').val('').trigger('change');
        });
    });
</script>