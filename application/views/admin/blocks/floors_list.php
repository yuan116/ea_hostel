<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="blocks_dtb" width="100%">
                        <thead>
                            <tr>
                                <th>Floor</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var blocks_dtb;
    $(document).ready(function() {
        blocks_dtb = $('#blocks_dtb').DataTable({
            "ajax"       : '<?php echo site_url("admin/blocks/get_list/{$block_id}"); ?>',
            "columns"    : [
                {data : 'name',       name : 'name'},
                {data : 'status',     name : 'status'},
                {data : 'fullname',   name : 'fullname'},
                {data : 'created_at', name : 'created_at'},
            ],
            "order"      : [[0, 'asc']],
            "columnDefs" : [
                {
                    'render': function(data, type, row){
                        return '<a href="<?php echo site_url("admin/rooms/list/"); ?>' + row['floor_id'] + '">' + data + '</a>';
                    },
                    "targets" : 0
                },

                {
                    'render': function(data, type, row){
                        var label = '';

                        label += '<div class="change_status">';
                        label += 		'<input type="checkbox" floor_id="' + row['floor_id'] + '" value="1" data-toggle="toggle" ';
                                    
                        if(data == "1"){
                            label +=			'checked ';
                        }

                        label +=		'data-on="Active" data-off="Inactive" data-onstyle="success btn-xs" data-offstyle="danger btn-xs"/>';
                        label += '</div>';

                        return label;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 1
                },
            ]
        });

        blocks_dtb.on('order.dt search.dt', function () {
            $('.change_status input[type="checkbox"]').bootstrapToggle({
                size  : 'small',
                width : '100'
            });
        }).draw();
    });

    $('table').on({change: function(e){
        var floor_id = $(this).attr('floor_id');
		var status = $(this).is(":checked") === true ? 1 : 0;
        
		$.ajax({
			type : 'POST',
			url  : '<?php echo site_url('admin/blocks/change_status'); ?>',
            data : {
                "floor_id" : floor_id,
                "status"   : status,
            },
        });
	}}, 'input[type="checkbox"]');
</script>