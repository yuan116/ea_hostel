<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li class="<?php echo $tab_active == 1 ? 'active' : ''; ?>"><a href="<?php echo site_url("admin/blocks/floors_list/{$block_data->block_id}"); ?>"><strong><?php echo $block_data->name; ?></strong> Floors List</a></li>
                <li><a href="<?php echo site_url("admin/blocks/add_floor/{$block_data->block_id}") ?>" class="btn btn-info btn-flat"><i class="fa fa-plus fa-fw"></i> Add Floor</a></li>
                <li class="pull-right"><a href="<?php echo site_url("admin/buildings/list"); ?>" class="btn btn-info btn-flat"><strong>Back</strong></a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <?php echo $tab_view; ?>
                </div>
            </div>
        </div>
    </div>
</div>