<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="rooms_dtb" width="100%">
                        <thead>
                            <tr>
                                <th>Room</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var rooms_dtb;
    $(document).ready(function() {
        rooms_dtb = $('#rooms_dtb').DataTable({
            "ajax"       : '<?php echo site_url("admin/rooms/get_list/{$floor_id}"); ?>',
            "columns"    : [
                {data : 'name',       name : 'name'},
                {data : 'status',     name : 'status'},
                {data : 'fullname',   name : 'fullname'},
                {data : 'created_at', name : 'created_at'},
            ],
            "order"      : [[0, 'asc']],
            "columnDefs" : [
                {
                    'render': function(data, type, row){
                        var label = '';

                        label += '<div class="change_status">';
                        label += 		'<input type="checkbox" room_id="' + row['room_id'] + '" value="1" data-toggle="toggle" ';

                        if(data == "1"){
                            label +=			'checked ';
                        }

                        label +=		'data-on="Active" data-off="Inactive" data-onstyle="success btn-xs" data-offstyle="danger btn-xs"/>';
                        label += '</div>';

                        return label;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 1
                },
            ]
        });

        rooms_dtb.on('order.dt search.dt', function () {
            $('.change_status input[type="checkbox"]').bootstrapToggle({
                size  : 'small',
                width : '100'
            });
        }).draw();
    });

    $('table').on({change: function(e){
        var room_id = $(this).attr('room_id');
		var status = $(this).is(":checked") === true ? 1 : 0;
        
		$.ajax({
			type : 'POST',
			url  : '<?php echo site_url('admin/rooms/change_status'); ?>',
            data : {
                "room_id" : room_id,
                "status"  : status,
            },
        });
	}}, 'input[type="checkbox"]');
</script>