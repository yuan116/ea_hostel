<div class="row">
    <div class="box box-default" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li class="<?php echo $tab_active == 1 ? 'active' : ''; ?>"><a href="<?php echo site_url("admin/rooms/list/{$room_data->floor_id}"); ?>"><strong><?php echo $room_data->name; ?></strong> Rooms List</a></li>
                <li><a href="<?php echo site_url("admin/rooms/add_room/{$room_data->floor_id}") ?>" class="btn btn-info btn-flat"><i class="fa fa-plus fa-fw"></i> Add Room</a></li>
                <li class="pull-right"><a href="<?php echo site_url("admin/blocks/floors_list/{$room_data->block_id}"); ?>" class="btn btn-info btn-flat"><strong>Back</strong></a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <?php echo $tab_view; ?>
                </div>
            </div>
        </div>
    </div>
</div>