<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="user_dtb" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Fullname</th>
                                <th>Gender</th>
                                <th>Handphone Number</th>
                                <th>Email</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var user_dtb;
    $(document).ready(function() {
        user_dtb = $('#user_dtb').DataTable({
            "ajax"       : '<?php echo site_url('admin/user/get_list'); ?>',
            "columns"    : [
                {data : null},
                {data : 'username',         name : 'username'},
                {data : 'description',      name : 'description'},
                {data : 'fullname',         name : 'fullname'},
                {data : 'gender',           name : 'gender'},
                {data : 'handphone_number', name : 'handphone_number'},
                {data : 'email',            name : 'email'},
                {data : 'created_at',       name : 'created_at'},
                {data : null}
            ],
            "order"      : [[3, 'asc']],
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 0
                },

                {
                    'render': function(data, type, row) {
                        var btn = '';

                        btn += '<a href="<?php echo site_url('admin/user/form'); ?>' + '/' + row['user_id'] + '" class="btn btn-xs btn-info btn-flat" title="Edit"><i class="fa fa-edit fa-fw"></i></a>&nbsp;';
                        btn += '<a href="<?php echo site_url('admin/user/delete'); ?>' + '/' + row['user_id'] + '" class="btn btn-xs btn-danger btn-flat" title="Delete" onclick="return delete_user();"><i class="fa fa-trash-o fa-fw"></i></a>';

                        return btn;
                    },
                    "searchable" : false,
                    "orderable"  : false,
                    "targets"    : 8
                },
            ]
        });

        user_dtb.on('order.dt search.dt', function () {
            user_dtb.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });

    function delete_user() {
        return window.confirm('Are you you want to delete?\nOnce delete this record cannot be recover.');
    }
</script>