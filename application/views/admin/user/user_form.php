<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <form role="form" method="post" autocomplete="off">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-11">
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo @$data->user_id; ?>" />

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="username">Username <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('username'); ?>">
                                <input type="text" class="form-control" name="username" id="username" maxlength="100" value="<?php echo set_value('username', @$data->username); ?>" <?php echo empty($data) ? '' : 'readonly'; ?> />
                                <?php echo form_error_label('username'); ?>
                            </div>
                        </div>

                        <?php if ($this->session->userdata('role_keyword') == ROLE_ADMINISTRATOR && @$data->role_keyword != ROLE_USER) : ?>
                            <div class="col-lg-12">
                                <label class="col-lg-2 control-label" for="role_keyword">Role <span class="text-red">*</span></label>
                                <div class="col-lg-8 form-group <?php echo form_has_error('role_keyword'); ?>">
                                    <select class="form-control select2" name="role_keyword" id="role_keyword">
                                        <option value="">Please Select</option>
                                        <option value="<?php echo ROLE_ADMINISTRATOR; ?>" <?php echo set_select('role_keyword', ROLE_ADMINISTRATOR, ROLE_ADMINISTRATOR == @$data->role_keyword); ?>>Administrator</option>
                                        <option value="<?php echo ROLE_USER_ADMIN; ?>" <?php echo set_select('role_keyword', ROLE_USER_ADMIN, ROLE_USER_ADMIN == @$data->role_keyword); ?>>User Admin</option>
                                    </select>
                                    <?php echo form_error_label('role_keyword'); ?>
                                </div>
                            </div>
                        <?php else : ?>
                            <input type="hidden" class="form-control" name="role_keyword" id="role_keyword" value="<?php echo @$data->role_keyword; ?>" readonly />
                        <?php endif; ?>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="fullname">Fullname <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('fullname'); ?>">
                                <input type="text" class="form-control" name="fullname" id="fullname" maxlength="255" value="<?php echo set_value('fullname', @$data->fullname); ?>" />
                                <?php echo form_error_label('fullname'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="gender">Gender <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('gender'); ?>">
                                <select class="form-control select2" name="gender" id="gender">
                                    <option value="">Please Select</option>
                                    <option value="M" <?php echo set_select('gender', 'M', 'M' == @$data->gender); ?>>Male</option>
                                    <option value="F" <?php echo set_select('gender', 'F', 'F' == @$data->gender); ?>>Female</option>
                                </select>
                                <?php echo form_error_label('gender'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="nric">Identity Card Number <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('nric'); ?>">
                                <input type="text" class="form-control" name="nric" id="nric" minlength="12" maxlength="12" value="<?php echo set_value('nric', @$data->nric); ?>" />
                                <p class="help-block">Please Enter Identity Card Number without '-'</p>
                                <?php echo form_error_label('nric'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="handphone_number">Handphone Number <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('handphone_number'); ?>">
                                <input type="text" class="form-control" name="handphone_number" id="handphone_number" minlength="10" maxlength="11" value="<?php echo set_value('handphone_number', @$data->handphone_number); ?>" />
                                <p class="help-block">Please Enter Handphone Number without '-'</p>
                                <?php echo form_error_label('handphone_number'); ?>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <label class="col-lg-2 control-label" for="email">Email <span class="text-red">*</span></label>
                            <div class="col-lg-8 form-group <?php echo form_has_error('email'); ?>">
                                <input type="email" class="form-control" name="email" id="email" maxlength="255" value="<?php echo set_value('email', @$data->email); ?>" />
                                <?php echo form_error_label('email'); ?>
                            </div>
                        </div>

                        <?php if (@$data->role_keyword == ROLE_USER) : ?>
                            <div class="text-center">
                                <h4><strong>Student University/College Information</strong></h4>
                            </div><br />

                            <div class="col-lg-12">
                                <label class="col-lg-2 control-label" for="student_id">Student ID <span class="text-red">*</span></label>
                                <div class="col-lg-8 form-group <?php echo form_has_error('student_id'); ?>">
                                    <input type="text" class="form-control" name="student_id" id="student_id" maxlength="10" value="<?php echo set_value('student_id', $data->student_id); ?>" />
                                    <?php echo form_error_label('student_id'); ?>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <label class="col-lg-2 control-label" for="school_name">University/College Name <span class="text-red">*</span></label>
                                <div class="col-lg-8 form-group <?php echo form_has_error('school_name'); ?>">
                                    <input type="text" class="form-control" name="school_name" id="school_name" maxlength="200" value="<?php echo set_value('school_name', $data->school_name); ?>" />
                                    <?php echo form_error_label('school_name'); ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="col-lg-12">
                            <div class="col-xs-2"></div>
                            <div class="col-lg-8 form-group">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        <?php if (empty($data)) : ?>
            $('#username').keyup(function() {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('admin/user/check_username'); ?>',
                    data: {
                        'username' : this.value,
                        'type'     : 'ajax'
                    },
                    success: function(data) {
                        $('#username').next('label').html(data.message);

                        if (data.result === true) {
                            $('#username').next('label').removeClass('text-danger');
                            $('button[type="submit"]').prop('disabled', false);
                        } else if (data.result === false) {
                            $('#username').next('label').addClass('text-danger');
                            $('button[type="submit"]').prop('disabled', true);
                        }
                    }
                });
            });
        <?php endif; ?>

        $('#nric, #handphone_number').keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;

            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        });
    });
</script>