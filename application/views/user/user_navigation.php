<?php $nav_active = $this->uri->segment(2); ?>

<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a href="<?php echo site_url(); ?>" class="navbar-brand"><b>EA</b> Hostel</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $nav_active == 'dashboard' ? 'active' : ''; ?>"><a href="<?php echo site_url('user/dashboard'); ?>"><i class="fa fa-list-alt fa-fw"></i> Dashboard</a></li>
                    <li class="<?php echo $nav_active == 'feedbacks' ? 'active' : ''; ?>"><a href="<?php echo site_url('user/feedbacks/form'); ?>"><i class="fa fa-envelope fa-fw"></i> Feedback</a></li>
                    <li class="<?php echo $nav_active == 'request_room' ? 'active' : ''; ?>"><a href="<?php echo site_url('user/request_room'); ?>"><i class="fa fa-bed fa-fw"></i> Room</a></li>
                </ul>
            </div>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" id="notification_dropdown" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label"></span>
                        </a>
                        <ul class="dropdown-menu" id="notification_menu">
                        </ul>
                    </li> -->

                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span><?php echo $this->session->userdata('fullname'); ?></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <ul class="menu">
                                    <li><a href="<?php echo site_url('user/profile'); ?>"><i class="fa fa-user fa-fw"></i> Profile</a></li>
                                    <li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<script>
    /* $(document).ready(function() {
        function load_notifications(seen = '') {
            $.ajax({
                'type': 'POST',
                'url': '<?php echo site_url('user/user/load_notifications'); ?>',
                'data': {
                    seen: seen,
                },
                'success': function(data) {
                    $('#notification_menu').html(data.notification);
                    if (data.unseen != 0) {
                        $('#notification_dropdown i.fa').removeClass('fa-bell-o').addClass('fa-bell');
                        $('#notification_dropdown span.label').addClass('label-warning').html(data.unseen);

                        //add toastr the notification too
                    }
                },
            });
        }

        $('#notification_dropdown').click(function() {
            $('#notification_dropdown i.fa').removeClass('fa-bell').addClass('fa-bell-o');
            $('#notification_dropdown span.label').removeClass('label-warning').html('');
            load_notifications(1);
        });

        //Refresh every 5 seconds
        setInterval(function() {
            load_notifications();
        }, 5000);

        load_notifications();
    }); */
</script>